package org.genesys2.server.model.elastic;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.util.MCPDUtil;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Collect {

	private Set<String> collCode;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String collDate;
	private String collInstAddr;
	private String collMissId;
	private String collName;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String collNumb;
	private String collSite;
	private Integer collSrc;
	private Date collectingDate;

	public static Collect from(AccessionCollect collect) {
		if (collect == null)
			return null;

		Collect c = new Collect();
		if (StringUtils.isNotBlank(collect.getCollCode()))
			c.collCode = new HashSet<String>(MCPDUtil.toList(collect.getCollCode()));
		c.collDate = StringUtils.defaultIfBlank(collect.getCollDate(), null);
		c.collInstAddr = StringUtils.defaultIfBlank(collect.getCollInstAddress(), null);
		c.collMissId = StringUtils.defaultIfBlank(collect.getCollMissId(), null);
		c.collName = StringUtils.defaultIfBlank(collect.getCollName(), null);
		c.collNumb = StringUtils.defaultIfBlank(collect.getCollNumb(), null);
		c.collSite = StringUtils.defaultIfBlank(collect.getCollSite(), null);
		c.collSrc = collect.getCollSrc();
		return c;
	}

	public Set<String> getCollCode() {
		return collCode;
	}

	public void setCollCode(Set<String> collCode) {
		this.collCode = collCode;
	}

	public String getCollDate() {
		return collDate;
	}

	public void setCollDate(String collDate) {
		this.collDate = collDate;
	}

	public String getCollInstAddr() {
		return collInstAddr;
	}

	public void setCollInstAddr(String collInstAddr) {
		this.collInstAddr = collInstAddr;
	}

	public String getCollMissId() {
		return collMissId;
	}

	public void setCollMissId(String collMissId) {
		this.collMissId = collMissId;
	}

	public String getCollName() {
		return collName;
	}

	public void setCollName(String collName) {
		this.collName = collName;
	}

	public String getCollNumb() {
		return collNumb;
	}

	public void setCollNumb(String collNumb) {
		this.collNumb = collNumb;
	}

	public String getCollSite() {
		return collSite;
	}

	public void setCollSite(String collSite) {
		this.collSite = collSite;
	}

	public Integer getCollSrc() {
		return collSrc;
	}

	public void setCollSrc(Integer collSrc) {
		this.collSrc = collSrc;
	}

	public Date getCollectingDate() {
		return collectingDate;
	}

	public void setCollectingDate(Date collectingDate) {
		this.collectingDate = collectingDate;
	}
}