<%@ tag description="ES term result" pageEncoding="UTF-8" %>
<%@ attribute name="termResult" required="true" type="java.lang.Object" %>
<%@ attribute name="type" required="false" type="java.lang.String" %>
<%@ attribute name="count" required="false" type="java.lang.Integer" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
  <c:when test="${count eq null or count eq 0}">
    <c:set var="totalCount" value="${termResult.totalCount+termResult.missingCount}" />
  </c:when>
  <c:otherwise>
    <c:set var="totalCount" value="${count}" />
  </c:otherwise>
</c:choose>

<table class="terms-table">
<c:forEach items="${termResult.terms}" var="term">
  <tr><td>
  <c:choose>
    <c:when test="${type=='country'}">
      <a href="<c:url value="/geo/${term.term.toUpperCase()}" />"><c:out value="${jspHelper.getCountry(term.term).getName(pageContext.response.locale)}" /></a>
    </c:when>
    <c:when test="${type=='crop'}">
      <c:out value="${jspHelper.getCrop(term.term).getName(pageContext.response.locale)}" />
    </c:when>
    <c:when test="${type=='instCode'}">
      <a href="<c:url value="/wiews/${term.term.toUpperCase()}" />"><c:out value="${term.term.toUpperCase()}" /></a>
    </c:when>
    <c:when test="${type=='bool' and term.term=='T'}">
      <spring:message code="boolean.true" />
    </c:when>
    <c:when test="${type=='bool' and term.term=='F'}">
      <spring:message code="boolean.false" />
    </c:when>
    <c:when test="${type ne null && type.startsWith('i18n')}">
      <spring:message code="${type.substring(5).concat('.').concat(term.term)}" />
    </c:when>
    <c:when test="${type=='genus'}">
      <a href="<c:url value="/acn/t/${term.term}" />"><span dir="ltr" class="sci-name" ><c:out value="${term.term}" /></span></a>
    </c:when>
    <c:when test="${type=='species'}">
      <span dir="ltr" class="sci-name" ><c:out value="${term.term}" /></span>
    </c:when>
    <c:otherwise><c:out value="${term.term}" /></c:otherwise>
  </c:choose>
  
  </td><td class="text-right"><fmt:formatNumber value="${term.count}" /> <span class="terms-percent"><fmt:formatNumber pattern="#,##0.00%" value="${term.count/totalCount}" /></span></td></tr>
</c:forEach>
<c:if test="${termResult.otherCount gt 0}">
  <tr><td><em><spring:message code="data-overview.otherCount" /></em></td><td class="text-right"><fmt:formatNumber value="${termResult.otherCount}" /> <span class="terms-percent"><fmt:formatNumber pattern="#,##0.00%" value="${termResult.otherCount/totalCount}" /></span></td></tr>
</c:if>
<c:if test="${termResult.missingCount gt 0}">
  <tr><td><em><spring:message code="data-overview.missingCount" /></em></td><td class="text-right"><fmt:formatNumber value="${termResult.missingCount}" /> <span class="terms-percent"><fmt:formatNumber pattern="#,##0.00%" value="${termResult.missingCount/totalCount}" /></span></td></tr>
</c:if>
<%-- <tr><td><spring:message code="data-overview.totalCount" /></td><td class="text-right">!<fmt:formatNumber value="${termResult.totalCount+termResult.missingCount}" /></td></tr> --%>
</table>

<c:remove var="totalCount" />