#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=Non autorisé
http-error.401.text=L'authentification est nécessaire.
http-error.403=Accès refusé
http-error.403.text=Vous n'avez pas l'autorisation d'accéder à cette ressource.
http-error.404=Non trouvé
http-error.404.text=La ressource demandée n'a pu être trouvée mais pourrait être de nouveau accessible à l'avenir.
http-error.500=Erreur interne au serveur
http-error.500.text=Un problème inattendu est survenu et aucun autre message spécifique ne convient.
http-error.503=Service indisponible
http-error.503.text=Le serveur est actuellement indisponible (étant surchargé ou en cours de maintenance).


# Login
login.username=Nom d'utilisateur
login.password=Mot de passe
login.invalid-credentials=Données de connexion invalides.
login.remember-me=Se souvenir de moi
login.login-button=Connexion
login.register-now=Créer un compte
logout=Déconnexion
login.forgot-password=Mot de passe oublié
login.with-google-plus=Se connecter avec Google+

# Registration
registration.page.title=Créer un compte utilisateur
registration.title=Créer votre compte
registration.invalid-credentials=Données de connexion invalides.
registration.user-exists=Nom d'utilisateur déjà utilisé.
registration.email=E-mail
registration.password=Mot de passe
registration.confirm-password=Confirmer le mot de passe
registration.full-name=Votre nom complet
registration.create-account=Créer un compte
captcha.text=Texte Captcha


id=ID

name=Nom
description=Description
actions=Actions
add=Ajouter
edit=Modifier
save=Sauvegarder
create=Créer
cancel=Annuler
delete=Supprimer
prompt.confirm-delete=Confirmez-vous la suppression du dossier sélectionné ?

jump-to-top=Revenir en haut de page

pagination.next-page=Suivant >
pagination.previous-page=< Précédent


# Language
locale.language.change=Modifier les paramètres régionaux
i18n.content-not-translated=Ce contenu n'est pas disponible dans votre langue. Merci de nous contacter si vous pouvez participer à sa traduction \!

data.error.404=Les données que vous avez demandées n'ont pas été trouvées dans le système.
page.rendertime=Le traitement de cette page a pris {0}ms.

footer.copyright-statement=&copy; 2013 - 2015 Fournisseurs de données et Crop Trust

menu.home=Accueil
menu.browse=Naviguer
menu.datasets=Données C&E
menu.descriptors=Descripteurs
menu.countries=Pays
menu.institutes=Instituts
menu.my-list=Ma liste
menu.about=À propos de Genesys
menu.contact=Nous contacter
menu.disclaimer=Clause de non-responsabilité
menu.feedback=Retour
menu.help=Aide
menu.terms=Conditions d'utilisation
menu.copying=Politique de droits d'auteur
menu.privacy=Politique de protection de la vie privée
menu.newsletter=Newsletter Genesys
menu.join-the-community=Rejoindre la communauté Genesys
menu.faq=Foire aux questions
menu.news=Actualités
page.news.title=Actualités
page.news.intro=Les toutes dernières actualités de la communauté Genesys \: des nouveaux membres aux mises à jour portant sur les accessions du monde entier.

# Extra content
menu.what-is-genesys=En quoi consiste Genesys?
menu.who-uses-genesys=Qui utilise Genesys?
menu.how-to-use-genesys=Comment utiliser Genesys ?
menu.history-of-genesys=Historique de Genesys
menu.about-genesys-data=À propos des données de Genesys



page.home.title=Genesys PGR

user.pulldown.administration=Administration
user.pulldown.users=Liste d'utilisateurs
user.pulldown.logout=Déconnexion
user.pulldown.profile=Mon profil
user.pulldown.oauth-clients=Clients OAuth
user.pulldown.teams=Équipes
user.pulldown.manage-content=Gérer le contenu

user.pulldown.heading={0}
user.create-new-account=Créer un compte
user.full-name=Nom complet
user.email=Adresse électronique
user.account-status=Statut du compte
user.account-disabled=Compte désactivé
user.account-locked-until=Compte verrouillé jusqu'à
user.roles=Rôles de l'utilisateur
userprofile.page.title=Profil d'utilisateur
userprofile.page.intro=De par son statut de communauté, la réussite de Genesys dépend de ses utilisateurs. Que vous soyez chercheur ou représentiez une institution de plus grande ampleur, vous avez la possibilité de mettre à jour votre profil afin de dévoiler vos intérêts et les données qui composent votre collection.
userprofile.update.title=Mettre à jour votre profil

user.page.list.title=Comptes d'utilisateurs enregistrés

crop.croplist=Liste de cultures
crop.all-crops=Toutes les cultures
crop.taxonomy-rules=Règles de taxonomie
crop.view-descriptors=Voir descripteurs de cultures...
crop.page.edit.title=Modifier {0}
crop.summary=Résumé (métadonnées HTML)

activity.recent-activity=Activité récente

country.page.profile.title=Profil du pays \: {0}
country.page.list.title=Liste de pays
country.page.list.intro=D'un simple clic sur le nom d'un pays, vous trouvez les instituts répertoriés dans ce pays ainsi que des statistiques sur les ressources phytogénétiques de ce dernier, dans son ensemble.
country.page.not-current=Ceci est une ancienne entrée.
country.page.faoInstitutes={0} instituts enregistrés dans WIEWS
country.stat.countByLocation={0} accessions dans les instituts de ce pays
country.stat.countByOrigin={0} accessions enregistrées dans Genesys proviennent de ce pays.
country.statistics=Statistiques du pays
country.accessions.from=Accessions collectées dans {0}
country.more-information=Plus d'informations \:
country.replaced-by=Le code pays est remplacé par \: {0}
country.is-itpgrfa-contractingParty={0} est partie au Traité international sur les ressources phytogénétiques pour l'alimentation et l'agriculture (TIRPAA).
select-country=Choisir le pays

project.page.list.title=Projets
project.page.profile.title={0}
project.code=Code du projet
project.name=Nom du projet
project.url=Site Web du projet
project.summary=Résumé (métadonnées HTML)
project.accessionLists=Listes d'accessions


faoInstitutes.page.list.title=Instituts WIEWS
faoInstitutes.page.profile.title=WIEWS {0}
faoInstitutes.stat.accessionCount=Accessions au sein de Genesys \:
faoInstitutes.stat.datasetCount=Jeux de données supplémentaires dans Genesys \:
faoInstitute.stat-by-crop=Cultures les plus représentées
faoInstitute.stat-by-genus=Genres les plus représentés
faoInstitute.stat-by-species=Espèces les plus représentées
faoInstitute.accessionCount={0} accessions
faoInstitute.statistics=Statistiques de l'institut
faoInstitutes.page.data.title=Accessions dans {0}
faoInstitute.accessions.at=Accessions à {0}
faoInstitutes.viewAll=Voir tous les instituts enregistrés
faoInstitutes.viewActiveOnly=Voir les instituts avec accessions dans Genesys
faoInstitute.no-accessions-registered=Merci de nous contacter si vous pouvez nous permettre d'obtenir des informations de cet institut.
faoInstitute.institute-not-current=Ce dossier est archivé.
faoInstitute.view-current-institute=Parcourir {0}.
faoInstitute.country=Pays
faoInstitute.code=Code WIEWS
faoInstitute.email=E-mail de contact
faoInstitute.acronym=Acronyme
faoInstitute.url=Lien web
faoInstitute.summary=Résumé (métadonnées HTML)
faoInstitute.member-of-organizations-and-networks=Organisations et réseaux \:
faoInstitute.uniqueAcceNumbs.true=Chaque numéro d'accession est unique pour cet institut.
faoInstitute.uniqueAcceNumbs.false=Le même numéro d'accession peut être utilisé dans des collections séparées de cet institut.
faoInstitute.requests.mailto=Adresse électronique pour les demandes de matériel \:
faoInstitute.allow.requests=Permettre les demandes de matériel
faoInstitute.sgsv=Code SGSV
faoInstitute.data-title=Banque de données contenant les informations relatives aux accessions maintenues à {0}
faoInstitute.overview-title=Aperçu des informations relatives aux accessions maintenues à {0}
faoInstitute.datasets-title=Données de caractérisation et d'évaluation fournies par {0}
faoInstitute.meta-description=Aperçu des ressources phytogénétiques maintenues au sein de collections à la banque de gènes {0} ({1})
faoInstitute.link-species-data=Afficher les informations sur les accessions {2} à {0} ({1})
faoInstitute.wiewsLink={0} détails concernant le site Web WIEWS de la FAO

view.accessions=Parcourir les accessions
view.datasets=Voir les jeux de données
paged.pageOfPages=Page {0} sur {1}
paged.totalElements={0} entrées

accessions.number={0} accessions
accession.metadatas=Données C&E
accession.methods=Données de caractérisation et d'évaluation
unit-of-measure=Unité de mesure

ce.trait=Trait
ce.sameAs=Identique à
ce.methods=Méthodes
ce.method=Méthode
method.fieldName=Champ dans BDD

accession.uuid=UUID
accession.accessionName=Numéro d'accession
accession.acceNumb=Numéro d'accession
accession.origin=Pays d'origine
accession.orgCty=Pays d'origine
accession.holdingInstitute=Institut détenteur
accession.institute.code=Institut détenteur
accession.holdingCountry=Lieu
accession.institute.country.iso3=Lieu
accession.taxonomy=Nom scientifique
accession.taxonomy.sciName=Nom scientifique
accession.genus=Genre
accession.taxonomy.genus=Genre
accession.species=Espèce
accession.taxonomy.species=Espèce
accession.subtaxa=Sous-taxons
accession.taxonomy.subtaxa=Sous-taxons
accession.crop=Nom de la culture
accession.crops=Nom de la culture
accession.otherNames=Aussi connu sous le nom de
accession.inTrust=Déposé auprès du Trust
accession.mlsStatus=Statut MLS
accession.duplSite=Institut du doublon de sécurité
accession.inSvalbard=Sécurité dupliquée au Svalbard
accession.inTrust.true=Cette accession respecte l'article 15 du Traité international sur les ressources phytogénétiques pour l'alimentation et l'agriculture.
accession.mlsStatus.true=Cette accession est reprise dans le système multilatéral du TIRPAA.
accession.inSvalbard.true=Sécurité dupliquée dans la Réserve mondiale de semences du Svalbard.
accession.not-available-for-distribution=L'accession n'est PAS disponible à des fins de distribution.
accession.this-is-a-historic-entry=Il s'agit d'un ancien dossier relatif à une accession.
accession.historic=Ancienne entrée
accession.available-for-distribution=L'accession est disponible à des fins de distribution.
accession.elevation=Altitude
accession.geolocation=Géolocalisation (lat., long.)

accession.storage=Type de stockage du germoplasme
accession.storage.=
accession.storage.10=Collection de semences
accession.storage.11=Collection de semences à court terme
accession.storage.12=Collection de semences à moyen terme
accession.storage.13=Collection de semences à long terme
accession.storage.20=Collection en champ
accession.storage.30=Conservation in vitro
accession.storage.40=Cryoconservation
accession.storage.50=Collection d'ADN
accession.storage.99=Autre

accession.breeding=Informations sur le sélectionneur
accession.breederCode=Code sélectionneur
accession.pedigree=Pedigree
accession.collecting=Informations sur la collection
accession.collecting.site=Emplacement du site de collecte
accession.collecting.institute=Institut de collecte
accession.collecting.number=Numéro de collecte
accession.collecting.date=Date de collecte de l'échantillon
accession.collecting.mission=ID mission de collecte
accession.coll.collMissId=ID de la mission de collecte
accession.collecting.source=Source de collecte/acquisition

accession.collectingSource.=
accession.collectingSource.10=Habitat sauvage
accession.collectingSource.11=Forêt ou zone boisée
accession.collectingSource.12=Zone arbustive
accession.collectingSource.13=Prairie
accession.collectingSource.14=Désert ou toundra
accession.collectingSource.15=Habitat aquatique
accession.collectingSource.20=Champ ou habitat en zone cultivée
accession.collectingSource.21=Champ
accession.collectingSource.22=Verger
accession.collectingSource.23=Arrière-cour, potager ou jardin familial (urbain, périurbain ou rural)
accession.collectingSource.24=Jachère
accession.collectingSource.25=Pâturage
accession.collectingSource.26=Magasin de ferme
accession.collectingSource.27=Aire de battage
accession.collectingSource.28=Parc
accession.collectingSource.30=Marché ou magasin
accession.collectingSource.40=Institut, station expérimentale, organisme de recherche, banque de gènes
accession.collectingSource.50=Société semencière
accession.collectingSource.60=Habitat envahi de mauvaises herbes, perturbé ou rudéral
accession.collectingSource.61=Bordure de route
accession.collectingSource.62=Bordure de champ
accession.collectingSource.99=Autre

accession.donor.institute=Institut donateur
accession.donor.accessionNumber=ID d'accession du donateur
accession.geo=Informations géographiques
accession.geo.datum=Coordonnées géographiques
accession.geo.method=Méthode de géoréférencement
accession.geo.uncertainty=Incertitude liée aux coordonnées
accession.geo.latitudeAndLongitude=Géolocalisation

accession.sampStat=Statut biologique de l'accession
accession.sampleStatus=Statut biologique de l'accession
accession.sampleStatus.=
accession.sampleStatus.100=Sauvage
accession.sampleStatus.110=Naturel
accession.sampleStatus.120=Semi-naturel/sauvage
accession.sampleStatus.130=Semi-naturel/semé
accession.sampleStatus.200=Envahissant
accession.sampleStatus.300=Cultivar traditionnel/Variété locale
accession.sampleStatus.400=Matériel de sélection/recherche
accession.sampleStatus.410=Lignée de sélectionneurs
accession.sampleStatus.411=Population synthétique
accession.sampleStatus.412=Hybride
accession.sampleStatus.413=Stock fondateur/Population de base
accession.sampleStatus.414=Lignée endogame
accession.sampleStatus.415=Isolement de la population
accession.sampleStatus.416=Sélection clonale
accession.sampleStatus.420=Stock génétique
accession.sampleStatus.421=Mutant
accession.sampleStatus.422=Stocks cytogénétiques
accession.sampleStatus.423=Autres stocks génétiques
accession.sampleStatus.500=Cultivar avancé ou amélioré
accession.sampleStatus.600=OGM
accession.sampleStatus.999=Autre

accession.availability=Disponibilité à des fins de distribution
accession.aliasType.ACCENAME=Nom de l'accession
accession.aliasType.DONORNUMB=Identifiant d'accession du donateur
accession.aliasType.BREDNUMB=Nom donné par le sélectionneur
accession.aliasType.COLLNUMB=Numéro de collecte
accession.aliasType.OTHERNUMB=Autres noms
accession.aliasType.DATABASEID=(ID base de données)
accession.aliasType.LOCALNAME=(Nom local)

accession.availability.=Inconnu
accession.availability.true=Disponible
accession.availability.false=Non disponible

accession.historic.true=Ancien
accession.historic.false=Actif
accession.acceUrl=URL d'accession supplémentaire
accession.scientificName=Nom scientifique

accession.page.profile.title=Profil d'accession \: {0}
accession.page.resolve.title=Plusieurs accessions trouvées
accession.resolve=Plusieurs accessions trouvées dans Genesys sous le nom «{0}». Sélectionnez-en une dans la liste.
accession.page.data.title=Parcourir le catalogue
accession.page.data.intro=Explorez les données relatives aux accessions grâce à Genesys. Afin d'affiner votre recherche, appliquez des filtres tels que le pays d'origine, la culture, l'espèce ou encore la latitude et la longitude du site de collecte.
accession.taxonomy-at-institute=Voir {0} à {1}

accession.svalbard-data=Données de duplication de la Réserve mondiale de semences du Svalbard
accession.svalbard-data.taxonomy=Nom scientifique déclaré
accession.svalbard-data.depositDate=Date de dépôt
accession.svalbard-data.boxNumber=Numéro de boîte
accession.svalbard-data.quantity=Quantité
accession.remarks=Remarques

accession.imageGallery=Images des accessions

taxonomy.genus=Genre
taxonomy.species=Espèce
taxonomy.taxonName=Nom scientifique
taxonomy-list=Liste taxonomique

selection.checkbox=Cliquez pour ajouter ou supprimer l'accession de votre liste
selection.page.title=Accessions sélectionnées
selection.page.intro=À mesure que vous parcourez les millions d'accessions proposées par Genesys, vous avez la possibilité de créer votre propre liste, vous permettant de garder une trace des résultats de vos recherches. Vos sélections y sont conservées afin que vous puissiez les consulter à votre gré.
selection.add=Ajouter {0} à la liste
selection.remove=Retirer {0} de la liste
selection.clear=Vider la liste
selection.reload-list=Recharger la liste
selection.map=Afficher la carte d'accession
selection.send-request=Soumettre une demande de germoplasme
selection.empty-list-warning=Vous n'avez ajouté aucune accession à la liste.
selection.add-many=Ajouter plusieurs accessions
selection.add-many.accessionIds=Lister les numéros des accessions (ACCENUMB) de la même manière que dans Genesys, séparés par une virgule, un point-virgule ou une nouvelle ligne.
selection.add-many.instCode=Code WIEWS de l'institut détenteur
selection.add-many.button=Ajouter à la liste de sélection

savedmaps=Se souvenir de la carte actuelle
savedmaps.list=Liste des cartes
savedmaps.save=Se souvenir de la carte
taxonomy.subtaxa=Sous-taxons

filter.enter.title=Saisir un titre de filtre
filters.page.title=Filtres de données
filters.view=Filtres actuels
filter.filters-applied=Vous avez appliqué des filtres.
filter.filters-not-applied=Vous pouvez filtrer les données.
filters.data-is-filtered=Les données sont filtrées.
filters.toggle-filters=Filtres
filter.taxonomy=Nom scientifique
filter.art15=Accession Art. 15 TIRPAA
filter.acceNumb=Numéro d'accession
filter.seqNo=Nombre séquentiel détecté
filter.alias=Nom d'accession
filter.crops=Nom de la récolte
filter.orgCty.iso3=Pays d'origine
filter.institute.code=Nom de l'institut détenteur
filter.institute.country.iso3=Pays de l'institut détenteur
filter.sampStat=Statut biologique de l'accession
filter.institute.code=Nom de l'institut détenteur
filter.geo.latitude=Latitude
filter.geo.longitude=Longitude
filter.geo.elevation=Altitude
filter.taxonomy.genus=Genre
filter.taxonomy.species=Espèce
filter.taxonomy.subtaxa=Sous-taxons
filter.taxSpecies=Espèce
filter.taxonomy.sciName=Nom scientifique
filter.sgsv=Sécurité dupliquée au Svalbard
filter.mlsStatus=Statut MLS de l'accession
filter.available=Disponible à des fins de distribution
filter.historic=Ancien dossier
filter.donorCode=Institut donateur
filter.duplSite=Site du doublon de sécurité
filter.download-dwca=Télécharger le fichier ZIP
filter.download-mcpd=Télécharger MCPD
filter.add=Affiner la recherche par catégorie
filter.additional=Affiner la recherche par trait
filter.apply=Appliquer
filter.close=Fermer
filter.remove=Retirer le filtre
filter.autocomplete-placeholder=Tapez plus de 3 caractères...
filter.coll.collMissId=ID de la mission de collecte
filter.storage=Type de stockage du germoplasme
filter.string.equals=Égal à
filter.string.like=Commence par
filter.inverse=Hors
filter.set-inverse=Exclure les valeurs sélectionnées
filter.internal.message.like=Comme {0}
filter.internal.message.between=Entre {0}
filter.internal.message.and={0} et {0}
filter.internal.message.more=Plus de {0}
filter.internal.message.less=Moins de {0}
filter.lists=Figure sur la liste des accessions

columns.add=Modifier les colonnes affichées
columns.apply=Appliquer
columns.availableColumns=Sélectionnez les colonnes à afficher, puis cliquez sur Appliquer
columns.latitudeAndLongitude=Latitude & Longitude
columns.collectingMissionID=ID de la mission de collecte

columns.acceNumb=Numéro d'accession
columns.scientificName=Nom scientifique
columns.orgCty=Pays d'origine
columns.sampStat=Statut biologique de l'accession
columns.instCode=Institut détenteur

search.page.title=Recherche par texte complet
search.no-results=Aucun résultat trouvé pour votre recherche.
search.input.placeholder=Recherche Genesys...
search.search-query-missing=Veuillez saisir les termes de votre recherche.
search.search-query-failed=Désolé, la recherche a échoué, erreur {0}
search.button.label=Recherche
search.add-genesys-opensearch=Se connecter à la recherche Genesys à l'aide du navigateur
search.section.accession=Accessions
search.section.article=Contenu
search.section.activitypost=Actualités
search.section.institute=Banques de gènes
search.section.country=Pays

admin.page.title=Administration Genesys 2
metadata.page.title=Jeux de données
metadata.page.intro=Parcourez les jeux de données d'évaluation et de caractérisation mises en ligne sur Genesys par des chercheurs et organismes de recherche. Pour chacun d'eux, les données sont téléchargeables au format Excel ou CSV.
metadata.page.view.title=Détails du jeu de données
metadata.download-dwca=Télécharger le fichier ZIP
page.login=Connexion

traits.page.title=Descripteurs
trait-list=Descripteurs


organization.page.list.title=Organisations et réseaux
organization.page.profile.title={0}
organization.slug=Acronyme de l'organisation ou réseau
organization.title=Nom complet
organization.summary=Résumé (métadonnées HTML)
filter.institute.networks=Réseau


menu.report-an-issue=Signaler un problème
menu.scm=Code source
menu.translate=Traduire Genesys

article.edit-article=Révision de l'article
article.slug=Slug de l'article (URL)
article.title=Titre de l'article
article.body=Corps de l'article
article.summary=Résumé (métadonnées HTML)
article.post-to-transifex=Publier sur Transifex
article.remove-from-transifex=Supprimer de Transifex
article.fetch-from-transifex=Chercher des traductions
article.transifex-resource-updated=La ressource a été correctement mise à jour sur Transifex.
article.transifex-resource-removed=La ressource a été correctement supprimée de Transifex.
article.transifex-failed=Une erreur est survenue durant l'échange de données avec Transifex
article.translations-updated=Données traduites correctement ajoutées à la ressource \!
article.share=Partager cet article

user.accession.list.saved-updated=Sauvegarde de votre liste d'accessions réussie.
user.accession.list.deleted=Suppression de votre liste d'accessions du serveur réussie.
user.accession.list.create-update=Sauvegarder
user.accession.list.delete=Supprimer
user.accession.list.title=Liste d'accessions

activitypost=Post d'activité
activitypost.add-new-post=Ajouter nouveau post
activitypost.post-title=Titre du poste
activitypost.post-body=Corps

blurp.admin-no-blurp-here=Pas d'autre description disponible.
blurp.blurp-title=Titre de la description
blurp.blurp-body=Contenus de la description
blurp.update-blurp=Sauvegarder la description


oauth2.confirm-request=Confirmer l'accès
oauth2.confirm-client=Vous, <b>{0}</b>, autorisez par la présente <b>{1}</b> à accéder à vos ressources protégées.
oauth2.button-approve=Oui, autoriser l'accès
oauth2.button-deny=Non, refuser l'accès

oauth2.authorization-code=Code d'autorisation
oauth2.authorization-code-instructions=Copier ce code d'autorisation \:

oauth2.access-denied=Accès refusé
oauth2.access-denied-text=Vous avez refusé l'accès à vos ressources.

oauth-client.page.list.title=Clients OAuth2
oauth-client.page.profile.title=Client OAuth2 \: {0}
oauth-client.active-tokens=Liste de jetons émis
client.details.title=Titre client
client.details.description=Description

maps.loading-map=Téléchargement de la carte...
maps.view-map=Voir la carte
maps.accession-map=Carte d'accession
maps.accession-map.intro=La carte vous affiche le site de collecte des accessions géo-référencées.
maps.baselayer.list=Modifier la vue de la carte

audit.createdBy=Créé par {0}
audit.lastModifiedBy=Dernière mise à jour par {0}

itpgrfa.page.list.title=Parties au TIRPAA

request.page.title=Demande de matériel aux instituts détenteurs
request.total-vs-available=Parmi les {0} accessions énumérées, {1} sont disponibles à des fins de distribution.
request.start-request=Demander le germoplasme disponible
request.your-email=Votre adresse électronique \:
request.accept-smta=Acceptation SMTA/MTA \:
request.smta-will-accept=J'accepte les conditions générales de SMTA/MTA
request.smta-will-not-accept=Je n'accepte PAS les conditions générales de SMTA/MTA
request.smta-not-accepted=Vous n'avez pas indiqué accepter les conditions générales de SMTA/MTA. Votre demande de matériel ne sera pas traitée.
request.purpose=Décrivez l'utilisation du matériel \:
request.purpose.0=Autre (veuillez donner des détails dans le champ Notes)
request.purpose.1=Recherche sur l'alimentation et l'agriculture
request.notes=Merci d'indiquer toute remarque supplémentaire \:
request.validate-request=Validez votre demande
request.confirm-request=Confirmation de réception de la demande
request.validation-key=Clé de validation \:
request.button-validate=Valider
validate.no-such-key=La clé de validation est invalide.

team.user-teams=Équipes de l'utilisateur
team.create-new-team=Créer une nouvelle équipe
team.team-name=Nom de l'équipe
team.leave-team=Quitter l'équipe
team.team-members=Membres de l'équipe
team.page.profile.title=Équipe \: {0}
team.page.list.title=Toutes les équipes
validate.email.key=Entrez la clé
validate.email=Validation par e-mail
validate.email.invalid.key=Clé invalide

edit-acl=Modifier les autorisations
acl.page.permission-manager=Gestionnaire d'autorisations
acl.sid=Identité de sécurité
acl.owner=Propriétaire de l'objet
acl.permission.1=Lire
acl.permission.2=Écrire
acl.permission.4=Créer
acl.permission.8=Supprimer
acl.permission.16=Gérer


ga.tracker-code=Code de suivi GA

boolean.true=Oui
boolean.false=Non
boolean.null=Inconnu
userprofile.password=Réinitialiser le mot de passe
userprofile.enter.email=Saisissez votre e-mail
userprofile.enter.password=Saisissez votre nouveau mot de passe
userprofile.email.send=Envoyer un e-mail

verification.invalid-key=La clé d'authentification est invalide.
verification.token-key=Clé de validation
login.invalid-token=Jeton d'authentification invalide

descriptor.category=Catégorie de descripteurs
method.coding-table=Tableau de codage

oauth-client.info=Infos client
oauth-client.list=Liste de clients OAuth
client.details.client.id=Coordonnées du client
client.details.additional.info=Informations supplémentaires
client.details.token.list=Liste de jetons
client.details.refresh-token.list=Liste de jetons d'actualisation
oauth-client.remove=Supprimer
oauth-client.remove.all=Tout supprimer
oauth-client=Client
oauth-client.token.issue.date=Date de publication
oauth-client.expires.date=Date d'expiration
oauth-client.issued.tokens=Jetons d'authentification émis
client.details.add=Ajouter client OAuth
oauth-client.create=Créer client OAuth
oauth-client.id=ID client
oauth-client.secret=Code secret client
oauth-client.redirect.uri=URI de redirection du client
oauth-client.access-token.accessTokenValiditySeconds=Validité du jeton d'authentification
oauth-client.access-token.refreshTokenValiditySeconds=Validité du jeton d'actualisation
oauth-client.access-token.defaultDuration=Utiliser la configuration par défaut
oauth-client.title=Titre client
oauth-client.description=Description
oauth2.error.invalid_client=ID client invalide. Validez vos paramètres client_id et client_secret.

team.user.enter.email=Saisir l'e-mail de l'utilisateur
user.not.found=Utilisateur non trouvé
team.profile.update.title=Mettre à jour les informations sur l'équipe

autocomplete.genus=Trouver un genre...

stats.number-of-countries={0} Pays
stats.number-of-institutes={0} Instituts
stats.number-of-accessions={0} Accessions
stats.number-of-historic-accessions={0} accessions historiques

navigate.back=Retour


content.page.list.title=Liste d'articles
article.lang=Langue
article.classPk=Objet

session.expiry-warning-title=Avertissement d'expiration de session
session.expiry-warning=Votre session actuelle est sur le point d'expirer. Souhaitez-vous prolonger cette session?
session.expiry-extend=Prolonger la session

download=Télécharger
download.kml=Télécharger le fichier KML


data-overview=Aperçu statistique
data-overview.intro=Un aperçu statistique des informations proposées par Genesys selon les filtres appliqués \: du nombre total d'accessions par pays, à la diversité des collections.
data-overview.short=Aperçu
data-overview.institutes=Instituts détenteurs
data-overview.composition=Composition du patrimoine des banques de gènes
data-overview.sources=Sources du matériel
data-overview.management=Gestion de la collection
data-overview.availability=Disponibilité du matériel
data-overview.otherCount=Autre
data-overview.missingCount=Non spécifié
data-overview.totalCount=Total
data-overview.donorCode=Code WIEWS de la FAO pour l'institut donateur
data-overview.mlsStatus=Disponible à des fins de distribution au titre du MLS


admin.cache.page.title=Aperçu du cache de l'application
cache.stat.map.ownedEntryCount=Entrées de cache détenues
cache.stat.map.lockedEntryCount=Entrées de cache verrouillées
cache.stat.map.puts=Nombre d'écritures vers le cache
cache.stat.map.hits=Nombre d'accès au cache

loggers.list.page=Liste des enregistreurs configurés
logger.add-logger=Ajouter un enregistreur
logger.edit.page=Page de configuration d'enregistreur
logger.name=Nom de l'enregistreur
logger.log-level=Niveau de journal
logger.appenders=Appenders de journal

menu.admin.index=Admin
menu.admin.loggers=Enregistreurs
menu.admin.caches=Caches
menu.admin.ds2=Jeux de données DS2
menu.admin.usermanagement=Gestion des utilisateurs
menu.admin.repository=Référentiel
menu.admin.repository.files=Gestionnaire des fichiers du référentiel
menu.admin.repository.galleries=Galeries d'images

news.content.page.all.title=Liste des actualités
news.archive.title=Archives des actualités

worldclim.monthly.title=Climat sur le site de collecte
worldclim.monthly.precipitation.title=Précipitations mensuelles
worldclim.monthly.temperatures.title=Températures mensuelles
worldclim.monthly.precipitation=Précipitations mensuelles [mm]
worldclim.monthly.tempMin=Température minimale [°C]
worldclim.monthly.tempMean=Température moyenne [°C]
worldclim.monthly.tempMax=Température maximale [°C]

worldclim.other-climate=Autres données climatiques

download.page.title=Avant de télécharger
download.download-now=Lancer le téléchargement, je vais patienter.

resolver.page.index.title=Trouver un identifiant permanent
resolver.acceNumb=Numéro d'accession
resolver.instCode=Code de l'institut détenteur
resolver.lookup=Trouver un identifiant
resolver.uuid=UUID
resolver.resolve=Résoudre

resolver.page.reverse.title=Résultats de la résolution
accession.purl=URL permanente

menu.admin.kpi=KPI
admin.kpi.index.page=KPI
admin.kpi.execution.page=Exécution des KPI
admin.kpi.executionrun.page=Détails de l'exécution

accession.pdci=Taux de rappel des données de passeport
accession.pdci.jumbo=Score PDCI \: {0,number,0.00} sur 10,0
accession.pdci.institute-avg=Le score PDCI moyen pour cet institut est de {0,number,0.00}
accession.pdci.about-link=En savoir plus sur le taux de rappel des données de passeport
accession.pdci.independent-items=Indépendant du type de population
accession.pdci.dependent-items=Selon le type de population
accession.pdci.stats-text=Le score PDCI moyen pour les accessions {0} est de {1,number,0.00}, avec un score minimal de {2,number,0.00} et un score maximal de {3,number,0.00}.

accession.donorNumb=Code de l'institut donateur
accession.acqDate=Date d'acquisition

accession.svalbard-data.url=URL de la banque de données du Svalbard
accession.svalbard-data.url-title=Informations relatives au dépôt au sein de la banque de données de la SGSV
accession.svalbard-data.url-text=Afficher les informations relatives au dépôt au sein de la SGSV pour {0}

filter.download-pdci=Télécharger les données PDCI
statistics.phenotypic.stats-text=Sur les {0} accessions, {1} accessions ({2,number,0.##%}) ont au moins un trait supplémentaire enregistré dans un jeu de données disponible sur Genesys (en moyenne {3,number,0.##} traits dans {4,number,0.##} jeux de données).

twitter.tweet-this=Tweeter \!
twitter.follow-X=Suivre @{0}
linkedin.share-this=Partager sur LinkedIn
share.link=Partager le lien
share.link.placeholder=Veuillez patienter...
share.link.text=Veuillez utiliser la version courte de l'URL complète vers cette page \:

welcome.read-more=En savoir plus sur Genesys
twitter.latest-on-twitter=Dernières actualités sur Twitter
video.play-video=Lire la vidéo

heading.general-info=Informations générales
heading.about=À propos
heading.see-also=Voir également
see-also.map=Carte des localités des accessions
see-also.overview=Aperçu de la collection
see-also.country=En savoir plus sur la conservation de la diversité phytogénétique à {0}

help.page.intro=Consultez la section dédiée aux tutoriels afin de découvrir comment utiliser Genesys.

charts=Cartes et graphiques
charts.intro=Vous trouverez dans cette rubrique des cartes et graphiques qui pourront être utiles à votre prochaine présentation \!
chart.collections.title=Taille des collections en banques de gènes
chart.attribution-text=www.genesys-pgr.org
chart.collections.series=Nombre d'accessions en banques de gènes

label.load-more-data=Charger plus...

userlist.list-my-lists=Listes d'accessions sauvegardées
userlist.title=Titre de la liste
userlist.description=Description de la liste
userlist.disconnect=Déconnecter la liste
userlist.update-list=Sauvegarder la liste mise à jour
userlist.make-new-list=Créer une nouvelle liste
userlist.shared=Autoriser d'autres personnes à accéder à la liste

region.page.list.title = Régions géographiques de la FAO
region.page.list.intro = Les listes de régions géographiques de la FAO ci-dessous vous permettent d'accéder aux données relatives aux accessions collectées et maintenues dans chaque région.
region.page.show.parent = Afficher la région mère {0}
region.page.show.world = Afficher toutes les régions du monde
region.countries-in-region=Liste des pays de {0}
region.regions-in-region=Régions FAO de {0}
region.show-all-regions=Liste de toutes les régions FAO

repository.file.path=Chemin d'accès
repository.file.originalFilename=Nom de fichier d'origine
repository.file.title=Titre
repository.file.subject=Sujet
repository.file.description=Description
repository.file.creator=Créateur
repository.file.created=Date de création
repository.file.rightsHolder=Détenteur des droits
repository.file.accessRights=Droits d'accès
repository.file.license=URL ou nom de licence
repository.file.format=Format
repository.file.contentType=Type de contenu
repository.file.extension=Extension de fichier
repository.file.extent=Zone
repository.file.bibliographicCitation=Référence bibliographique
repository.file.dateSubmitted=Date de soumission
repository.file.lastModifiedDate=Date de dernière modification

file.upload-file=Charger un fichier
file.upload-file.help=Sélectionner un fichier à charger depuis l'ordinateur local

repository.gallery=Galerie d'images
repository.gallery.title=Titre de la galerie d'images \:
repository.gallery.description=Description de la galerie d'images \:
repository.gallery.path=Chemin d'accès aux images de la galerie au sein du référentiel \:
repository.gallery.successfully-updated=Galerie d'images mise à jour avec succès \!
repository.gallery.removed=Galerie d'images supprimée.
repository.gallery.create-here=Créer une galerie d'images
repository.gallery.navigate=Afficher la galerie d'images
repository.gallery.downloadImage=Télécharger une image

welcome.search-genesys=Recherche sur Genesys
welcome.networks=Réseaux

