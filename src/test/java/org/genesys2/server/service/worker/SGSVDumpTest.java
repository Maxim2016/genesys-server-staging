/*
 * Copyright 2016 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.genesys2.server.service.worker;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;

/**
 * SGSV data:
 * 
 * <ul>
 * <li>INSTCODE, ACCENUMB are not unique in SGSV. See USA996 accession PI89009 or COL003:G10276</li>
 * </ul>
 * 
 * @author mobreza
 *
 */
@Ignore
public class SGSVDumpTest {

	@Test
	public void download() {
		// Set<String> instCodeAcceNumb = new HashSet<>();
		SGSVUpdate updater = new SGSVUpdate() {

			@Override
			void workIt(List<String[]> bulk) {
				LOG.trace("Queueing job size=" + bulk.size());
				final List<SGSVEntry> accns = bulkToList(bulk);
				for (SGSVEntry entry : accns) {
					if (StringUtils.isBlank(entry.acceNumb)) {
						LOG.warn("No ACCENUMB for entry=" + entry);
					}
					// String key = entry.instCode + ":" + entry.acceNumb + ":" + entry.genus;
					// if (instCodeAcceNumb.contains(key)) {
					// LOG.warn("Duplicate entry for " + key + " entry=" + entry);
					// }
					// instCodeAcceNumb.add(key);
				}
			}
		};
		updater.updateSGSV();
	}

}
