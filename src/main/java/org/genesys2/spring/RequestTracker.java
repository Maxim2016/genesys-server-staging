package org.genesys2.spring;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class RequestTracker implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -795441001900977822L;
	private String lastGet;

	public String getLastGet() {
		return lastGet;
	}

	public void setLastGet(String lastGet) {
		this.lastGet = lastGet;
	}
}
