<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="mini" tagdir="/WEB-INF/tags/mini" %>

<html>
<head>
  <title><spring:message code="search.page.title" /></title>
</head>
<body>
<h1><spring:message code="search.page.title" /></h1>

<div class="main-col-header clearfix">
  <div class="nav-header">
    <div class="results"><spring:message code="paged.totalElements" arguments="${pagedData == null ? 0 : pagedData.totalElements}" /></div>
    <form method="get" action="search2">
      <input type="hidden" name="q" value="<c:out value="${q}" />" />
      <div class="pagination">
        <spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
		<c:url value="" var="baseLink"><c:param name="q" value="${q}" /><c:param name="section" value="${section}" /></c:url>
        <a class="${pagedData.number eq 0 ? 'disabled' :''}" href="${baseLink}&amp;page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message code="pagination.previous-page" /></a>
        <input class="form-control" style="display: inline; max-width: 5em; text-align: center" type="text" name="page" placeholder="${pagedData.number + 1}" />
        <a href="<c:url value="${baseLink}"><c:param name="page" value="${pagedData.number + 2}" /></c:url>"><spring:message code="pagination.next-page" /></a>
      </div>
    </form>
  </div>
</div>

<!-- Alerts? -->
<gui:alert type="warning" display="${error ne null}">
  <spring:message code="search.search-query-failed" arguments="${error.message}" />
</gui:alert>
<gui:alert type="warning" display="${pagedData eq null}">
  <spring:message code="search.search-query-missing" />
</gui:alert>

<div class="applied-filters">
  <form class="" method="get" action="<c:url value="/acn/search2" />">
  	<input type="hidden" name="section" value="<c:out value="${section}" />" />
    <div class="row">
      <div class="col-md-4"><input type="text" placeholder="<spring:message code="search.input.placeholder" />" name="q" class="form-control" value="<c:out value="${q}" />" /></div>
      <div class="col-md-2"><input type="submit" value="<spring:message code="search.button.label" />" class="btn" /></div>
    </div>
  </form>
</div>

<c:set var="sectionList">accession,article,activitypost,country,institute</c:set>

<ul class="nav nav-tabs">
  <c:forTokens items="${sectionList}" delims="," var="sect">
    <li class=${sect eq section ? "active" : ""}>
      <a href="<c:url value="/acn/search2"><c:param name="q" value="${q}" /><c:param name="section" value="${sect}" /></c:url>"><spring:message code="search.section.${sect}" /></a>
    </li>
  </c:forTokens>
</ul>

<c:choose>
  <c:when test="${pagedData ne null and pagedData.totalElements gt 0}">
    <table class="accessions">
      <thead>
      <tr>
        <c:if test="${section eq 'accession'}">
          <td />
          <td><spring:message code="accession.accessionName" /></td>
          <td><spring:message code="accession.taxonomy" /></td>
          <td class="notimportant"><spring:message code="accession.origin" /></td>
          <td class="notimportant"><spring:message code="accession.sampleStatus" /></td>
          <td class="notimportant"><spring:message code="accession.holdingInstitute" /></td>
        </c:if>
        <c:if test="${section eq 'article'}">
        </c:if>
        <c:if test="${section eq 'activitypost'}">
        </c:if>
        <c:if test="${section eq 'country'}">
        </c:if>
        <c:if test="${section eq 'institute'}">
        </c:if>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${pagedData.content}" var="unit" varStatus="status">
        <tr class="acn">
          <c:choose>
            <c:when test="${section eq 'accession'}">
              <td class="sel" x-aid="${unit.id}"></td>
              <td><a href="<c:url value="/acn/id/${unit.id}" />"><b>
                <c:out value="${unit.acceNumb}" />
              </b></a></td>
              <td><c:out value="${unit.taxonomy.sciName}" /></td>
              <td class="notimportant"><c:out value="${jspHelper.getCountry(unit.orgCty.iso3).getName(pageContext.response.locale)}" /></td>
              <td class="notimportant"><spring:message code="accession.sampleStatus.${unit.sampStat}" /></td>
              <td class="notimportant"><a href="<c:url value="/wiews/${unit.institute.code}" />"><c:out value="${unit.institute.code}" /></a></td>
            </c:when>
            <c:when test="${section eq 'country'}">
            	<td><mini:country country="${unit}" /></td>
            </c:when>
            <c:when test="${section eq 'institute'}">
            	<td><mini:institute institute="${unit}" /></td>
            </c:when>
            <c:when test="${section eq 'article'}">
            	<td><mini:article article="${unit}" /></td>
            </c:when>
            <c:when test="${section eq 'activitypost'}">
            	<td><mini:activitypost activityPost="${unit}" /></td>
            </c:when>
            <c:otherwise>
              <td>
              	<b><c:out value="${section}" /></b>
                <a href="<c:url value="${unit.urlToContent}" />">
                  <c:set var="body" value="${unit.body}" />
                  <c:if test="${fn:length(body) > 100}">
                    <c:set var="body" value="${fn:substring(body, 0, 100)}..." />
                  </c:if>
                  <b><c:out value="${body}" escapeXml="false" /></b>
                </a>
              </td>
              <td><c:out value="${unit.summary}" escapeXml="false" /></td>
            </c:otherwise>
          </c:choose>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </c:when>
  <c:otherwise>
    <gui:alert type="info">
      <spring:message code="search.no-results" />
    </gui:alert>
  </c:otherwise>
</c:choose>
</body>
</html>