package org.genesys2.server.model.dataset;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class DSRowQualifierLong extends DSRowQualifier<Long> {

	@Column(name = "vall", updatable=false)
	private Long value;

	@Override
	public Long getValue() {
		return this.value;
	}

	@Override
	public void setValue(Long value) {
		this.value = value;
	}

}
