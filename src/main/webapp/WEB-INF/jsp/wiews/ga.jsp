<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${faoInstitute.settings['googleAnalytics.tracker'] ne null}">
_pageTrackers=["${faoInstitute.settings['googleAnalytics.tracker'].value}"];
</c:if>
