[[chRunning]]

== Running Genesys

Genesys requires a working connection to a RDMBS. See Genesys Server <<database-configuration>> for information how to select 
and configure the most appropriate database.

Setting up and running a server involves the following steps:

. <<installJetty,Download and install Jetty>>
. Obtain or <<quickBuild,build>> genesys2-server distribution package
. <<configureJetty,Configure Jetty for genesys2-server>>
. <<daemontools,Run Jetty as a service>> with daemontools

[[installJetty]]
=== Download and install Jetty

Download a **stable-9** copy of http://download.eclipse.org/jetty/[jetty] and unpack it 
to your favorite location.

In the next few lines, you will download, unpack and move a Jetty distribution to /opt folder.
File ownership and permissions need to be adjusted.

[source,bash,linenums]
----
$ cd /tmp
$ wget http://download.eclipse.org/jetty/stable-9/dist/jetty-distribution-9.3.6.v20151106.zip
# unzip the jetty distribution in the /tmp folder
$ unzip jetty-distribution-9.3.6.v20151106.zip
$ ls -la jetty-distribution-9.3.6.v20151106/

# as root, move the distribution to /opt
$ cd /opt
$ sudo mv /tmp/jetty-distribution-9.3.6.v20151106 /opt/jetty

# Adjust ownership
$ sudo chown -R root.nogroup /opt/jetty

# Adjust permissions
$ sudo chmod -R g-w /opt/jetty
$ sudo chmod -R o-rwx /opt/jetty
----

[[quickBuild]]
=== Building the project

At the moment of writing, the `{projectVersion}` is {projectVersion}.

Obtain a copy of `genesys2-server-{projectVersion}-jetty.zip` archive by 
building the project. The zip can be found in target/ directory.


[[configureJetty]]
=== Install webapp and configure Jetty

Unpack `genesys2-server-{projectVersion}-jetty.zip` archive and move its contents into the jetty directory,
next to existing `demo-base` and `webapps` directories.

[source,bash,linenums]
----
$ cd /opt/jetty
$ sudo unzip .../genesys2-server-{projectVersion}-jetty.zip
$ ls -la genesys2-server-{projectVersion}-jetty/
----

Your customized configuration options go to `genesys2-server-\\{projectVersion}-jetty/resources/genesys.properties`.

[source,bash,linenums]
----
$ sudo nano genesys2-server-{projectVersion}-jetty/resources/genesys.properties
----

Check <<genesys-configuration-options>> for the list of options.

Start jetty from the `genesys2-server-{projectVersion}-jetty` base

[source,bash,linenums]
----
$ cd genesys2-server-{projectVersion}-jetty/
$ java -jar ../start.jar
----

Standard output stream (stdout) is used for logging.

This configuration uses HSQL database and is intended for testing genesys2-server.
To change the database settings, edit `genesys2-server-{projectVersion}-jetty/resources/genesys.properties` file:

[source,properties,linenums]
----
# Genesys configuration with mysql database
db.url=jdbc:mysql://localhost/genesys?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
db.driverClassName=com.mysql.jdbc.Driver
db.username=root
db.password=mysql
hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect

----

All available configuration options and their default values are listed in <<genesys-configuration-options>>.


=== Building from source

Building and running the server requires https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html[Java 8 SDK],
https://maven.apache.org/[maven] and http://compass-style.org/install/[compass].

. Clone https://bitbucket.org/genesys2/genesys2-server.git[genesys2-server] to your computer
. Create a blank mysql database <<creating-a-mysql-database>>
. Configure database connection settings <<database-configuration>> in `src/main/resources/genesys.properties`
. Start Jetty with `mvn -Dspring.profiles.active=dev jetty:run`

