/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.model;

import org.springframework.data.domain.Page;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * This simple wrapper for ignoring some properties of
 * org.springframework.data.domain.Page, f.e. "iterator"
 *
 * NOTE: if you want to rewrite properties names, you'll need to specify all
 * properties explicitly
 */
@JsonRootName("result")
@JsonIgnoreProperties({ "iterator" })
public class PageList<T> {

	@JsonUnwrapped
	private final Page<T> page;

	public PageList(Page<T> page) {
		this.page = page;
	}

	public Page<T> getPage() {
		return page;
	}
}
