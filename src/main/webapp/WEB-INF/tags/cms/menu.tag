<%@ tag description="Display menu with menu items" pageEncoding="UTF-8" %>
<%@ tag body-content="tagdependent" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="items" required="true" type="java.util.Collection" %>
<%@ attribute name="key" required="true" type="java.lang.String" %>

<c:if test="${items ne null and items.size() gt 0}">
<ul class="list-unstyled cms cms-menu cms-menu-${key}">
	<c:forEach items="${menu.items}" var="menuItem" varStatus="status">
		<li class="cms cms-menu-item">
			<a href="<c:url value="${menuItem.url}" />" title="<spring:message code="${menuItem.title}" />"
				target="<c:out value="${menuItem.target}" />"
			>
				<spring:message code="${menuItem.text}" />
			</a>
		</li>
	</c:forEach>
</ul>
</c:if>