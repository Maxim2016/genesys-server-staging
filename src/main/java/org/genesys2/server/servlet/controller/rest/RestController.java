/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.impl.RESTApiException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

public abstract class RestController {
	protected final Log LOG = LogFactory.getLog(getClass());

	protected static final String JSON_OK = "{\"result\":true}";

	public RestController() {
		super();
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ExceptionJson handleIOException(Exception ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		LOG.error(ex, ex);
		response.setStatus(400); // ?
		return new ExceptionJson(ex);
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseBody
	public ExceptionJson handleAccessDeniedException(Exception ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		LOG.warn(request.getRequestURI() + " " + ex.getMessage() + " for "
				+ (authentication != null ? ((AuthUserDetails) authentication.getPrincipal()).getUsername() : "null"));
		response.setStatus(404); // ?
		return new ExceptionJson(ex);
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	public ExceptionJson handleHttpRequestMethodNotSupportedException(Exception ex, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		LOG.warn(request.getRequestURI() + " " + ex.getMessage() + " for "
				+ (authentication != null ? ((AuthUserDetails) authentication.getPrincipal()).getUsername() : "null"));
		response.setStatus(400); // ?
		return new ExceptionJson(ex);
	}

	@ExceptionHandler(RESTApiException.class)
	@ResponseBody
	public ExceptionJson handleRESTApiException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		LOG.warn(request.getRequestURI() + " " + ex.getMessage() + " for "
				+ (authentication != null ? ((AuthUserDetails) authentication.getPrincipal()).getUsername() : "null"));
		response.setStatus(400); // ?
		return new ExceptionJson(ex);
	}

}
