<%@ tag description="Display pagination block" pageEncoding="UTF-8" %>
<%@ tag body-content="tagdependent" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="page" required="true"
	type="org.springframework.data.domain.Page" %>
<%@ attribute name="action" required="false" type="java.lang.String" %>
<%--
	This is the more civil <nav> version of the pagination
--%>
<nav class="text-center pull-left">
	<ul class="pagination pagination-lg">
		<li><span><spring:message code="paged.totalElements"
					arguments="${page.totalElements}" /></span></li>

		<c:if test="${page.number eq 0}">
			<li class="disabled"><span
				aria-label="<spring:message
						code="pagination.previous-page" />">
					<span aria-hidden="true">&laquo;</span>
			</span></li>
			<li class="disabled"><span
				aria-label="<spring:message
						code="pagination.previous-page" />">
					<span aria-hidden="true">&lsaquo;</span>
			</span></li>
		</c:if>

		<c:if test="${page.number gt 0}">
			<li><a href="?"
				aria-label="<spring:message
						code="pagination.previous-page" />">
					<span aria-hidden="true">&laquo;</span>
			</a></li>
			<li><a
				href="<spring:url value=""><spring:param name="page" value="${page.number eq 0 ? 1 : page.number}" /></spring:url>"
				aria-label="<spring:message
					code="pagination.previous-page" />">&lsaquo;</a></li>
		</c:if>

		<li><span class="pagination-input">
				<form method="get" action="">
					<input class="text-center" type="text" name="page"
						placeholder="<c:out value="${page.number + 1}" />" value="<c:out value="${page.number + 1}" />" />
				</form>
		</span></li>

		<c:if test="${page.number ge page.totalPages-1}">
			<li class="disabled"><span aria-label="<spring:message code="pagination.next-page" />">&rsaquo;</span></li>
			<li class="disabled"><span aria-label="<spring:message code="pagination.next-page" />">&raquo;</span></li>
		</c:if>
		<c:if test="${page.number lt page.totalPages-1}">
			<li><a
				href="<spring:url value=""><spring:param name="page" value="${page.number+2}" /></spring:url>" aria-label="<spring:message
						code="pagination.next-page" />">&rsaquo;</a></li>
			<li><a
				href="<spring:url value=""><spring:param name="page" value="${page.totalPages}" /></spring:url>" aria-label="<spring:message
						code="pagination.next-page" />">&raquo;</a></li>
		</c:if>
			<li><span><spring:message code="paged.pageOfPages" arguments="${page.number+1},${page.totalPages}" /></span></li>
	</ul>
</nav>