package org.genesys2.server.model.elastic;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Remark {

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String field;
	private String remark;

	public static Remark from(AccessionRemark ar) {
		Remark r = new Remark();
		r.field = StringUtils.defaultIfBlank(ar.getFieldName(), null);
		r.remark = StringUtils.defaultIfBlank(ar.getRemark(), null);
		return r;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
}