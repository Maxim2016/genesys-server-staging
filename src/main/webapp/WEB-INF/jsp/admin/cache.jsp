<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="admin.cache.page.title" />1</title>
</head>
<body>

	<div class="row row-header">
		<div class="col-sm-7 col-xs-12"></div>
		<div class="col-sm-1 col-sm-offset-0 col-xs-offset-2 col-xs-2">
			<spring:message code="cache.stat.map.ownedEntryCount" />
		</div>
		<div class="col-sm-1 col-xs-2">
			<spring:message code="cache.stat.map.lockedEntryCount" />
		</div>
		<div class="col-sm-1 col-xs-2">
			<spring:message code="cache.stat.map.puts" />
		</div>
		<div class="col-sm-1 col-xs-2">
			<spring:message code="cache.stat.map.hits" />
		</div>
		<div class="col-sm-1 col-xs-2">
			<form class="form-inline" method="post"
				action="<c:url value="/admin/cache/clearCache" />">
				<button type="submit" name="clearAll" class="btn btn-warning">Clear</button>
				<!-- CSRF protection -->
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		</div>
	</div>

	<div class="rows-striped">
		<c:forEach items="${cacheMaps}" var="cacheMap">
			<div class="row">
				<div class="col-sm-7 col-xs-12">
					<div class="form-control-static">
						<b> <c:out value="${cacheMap.name}" />
						</b> <small> <c:out value="${cacheMap.serviceName}" />
						</small>
					</div>
				</div>

				<div class="col-sm-1 col-sm-offset-0 col-xs-offset-2 col-xs-2">
					<p class="form-control-static">
						<c:out value="${cacheMap.mapStats.ownedEntryCount}" />
					</p>
				</div>
				<div class="col-sm-1 col-xs-2">
					<p class="form-control-static">
						<c:out value="${cacheMap.mapStats.lockedEntryCount}" />
					</p>
				</div>
				<div class="col-sm-1 col-xs-2">
					<p class="form-control-static">
						<c:out value="${cacheMap.mapStats.putOperationCount}" />
					</p>
				</div>
				<div class="col-sm-1 col-xs-2">
					<p class="form-control-static">
						<c:out value="${cacheMap.mapStats.hits}" />
					</p>
				</div>

				<div class="col-sm-1 col-xs-2">
					<form method="post"
						action="<c:url value="/admin/cache/clearCache" />">
						<input type="hidden" name="name" value="${cacheMap.name}" />
						<button type="submit" class="btn btn-default btn-sm">Clear</button>
						<!-- CSRF protection -->
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form>
				</div>


			</div>
		</c:forEach>
	</div>

	<c:forEach items="${cacheOthers}" var="cacheOther">
		<h3>
			<c:out value="${cacheOther}" />
			<c:out value="${cacheOther}" />
		</h3>
	</c:forEach>
</body>
</html>