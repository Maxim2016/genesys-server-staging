package org.genesys2.server.servlet.controller.rest;

import java.util.ArrayList;
import java.util.List;

import org.genesys2.server.model.impl.GeoRegion;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.service.GeoRegionService;
import org.genesys2.server.servlet.controller.rest.model.GeoJsData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = {"/api/v0/geo/regions", "/json/v0/geo/regions"})
public class GeoController extends RestController {

    @Autowired
    GeoRegionService geoRegionService;

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    CountryRepository countryRepository;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public
    @ResponseBody
    Object getAllGeoRegions() {


        List<GeoRegion> regions = geoRegionService.findAll();
        List<GeoJsData> geoData = new ArrayList<>();

        for (GeoRegion region : regions) {

            GeoJsData jsData = new GeoJsData();
            jsData.setId(region.getIsoCode());
            jsData.setText(region.getName(LocaleContextHolder.getLocale()));

            if (region.getParentRegion() != null) {
                jsData.setParent(region.getParentRegion().getIsoCode());
            } else {
                jsData.setParent("#");
            }

            GeoRegion geoRegion = geoRegionService.find(region.getIsoCode());

            if (geoRegion.getCountries().size() > 0) {
                jsData.setHref("/geo/regions/" + region.getIsoCode());
            }

            geoData.add(jsData);
        }

        return geoData;
    }

    @RequestMapping(value = "/{isoCode}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public
    @ResponseBody
    Object getGeoRegions(@PathVariable(value = "isoCode") String code) {

        GeoRegion geoRegion = geoRegionService.find(code);

        List<GeoRegion> regions = geoRegionService.getChildren(geoRegion.getIsoCode());

        List<GeoJsData> geoData = new ArrayList<>();

        GeoJsData currentRegion = new GeoJsData();
        currentRegion.setId(geoRegion.getIsoCode());
        currentRegion.setText(geoRegion.getName(LocaleContextHolder.getLocale()));
        currentRegion.setParent("#");
        geoData.add(currentRegion);

        for (GeoRegion region : regions) {

            GeoJsData jsData = new GeoJsData();
            jsData.setId(region.getIsoCode());
            jsData.setText(region.getName(LocaleContextHolder.getLocale()));

            if (region.getParentRegion() != null) {
                jsData.setParent(region.getParentRegion().getIsoCode());
            } else {
                jsData.setParent("#");
            }

            GeoRegion childRegion = geoRegionService.find(region.getIsoCode());

            if (childRegion.getCountries().size() > 0) {
                jsData.setHref("/geo/regions/" + region.getIsoCode());
            }

            geoData.add(jsData);
        }

        return geoData;
    }

}