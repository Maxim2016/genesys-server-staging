/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.kpi.Dimension;
import org.genesys2.server.model.kpi.DimensionKey;
import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionRun;
import org.genesys2.server.model.kpi.JpaDimension;
import org.genesys2.server.model.kpi.KPIParameter;
import org.genesys2.server.model.kpi.Observation;
import org.genesys2.server.persistence.domain.kpi.DimensionKeyRepository;
import org.genesys2.server.persistence.domain.kpi.DimensionRepository;
import org.genesys2.server.persistence.domain.kpi.ExecutionRepository;
import org.genesys2.server.persistence.domain.kpi.ExecutionRunRepository;
import org.genesys2.server.persistence.domain.kpi.KPIParameterRepository;
import org.genesys2.server.persistence.domain.kpi.ObservationRepository;
import org.genesys2.server.service.KPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class KPIServiceImpl implements KPIService {
	public static final Log LOG = LogFactory.getLog(KPIServiceImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private DimensionRepository dimensionRepository;

	@Autowired
	private KPIParameterRepository parameterRepository;

	@Autowired
	private ExecutionRepository executionRepository;

	@Autowired
	private ObservationRepository observationRepository;

	@Autowired
	private DimensionKeyRepository dimensionKeyRepository;

	@Autowired
	private ExecutionRunRepository executionRunRepository;

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#parameter, 'ADMINISTRATION')")
	@Override
	@Transactional
	public KPIParameter save(KPIParameter parameter) {
		return parameterRepository.save(parameter);
	}

	@Override
	public KPIParameter getParameter(long id) {
		return parameterRepository.findOne(id);
	}

	@Override
	public KPIParameter getParameter(String name) {
		return parameterRepository.findByName(name);
	}

	@Override
	public List<KPIParameter> listParameters() {
		return parameterRepository.findAll(new Sort("name"));
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#parameter, 'ADMINISTRATION')")
	@Override
	@Transactional
	public KPIParameter delete(KPIParameter parameter) {
		parameterRepository.delete(parameter);
		parameter.setId(null);
		return parameter;
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#dimension, 'ADMINISTRATION')")
	@Override
	@Transactional
	public Dimension<?> save(Dimension<?> dimension) {
		LOG.debug("Persising dimension " + dimension);
		return dimensionRepository.save(dimension);
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#dimension, 'ADMINISTRATION')")
	@Override
	@Transactional
	public Dimension<?> delete(Dimension<?> dimension) {
		dimensionRepository.delete(dimension);
		dimension.setId(null);
		return dimension;
	}

	@Override
	public Dimension<?> getDimension(long id) {
		Dimension<?> dim = dimensionRepository.findOne(id);
		if (!(dim instanceof JpaDimension))
			dim.getValues().size();
		return dim;
	}

	@Override
	public List<Dimension<?>> listDimensions() {
		return dimensionRepository.findAll(new Sort("name"));
	}

	@Override
	public Execution getExecution(long id) {
		return executionRepository.findOne(id);
	}

	@Override
	public Execution getExecution(String executionName) {
		return executionRepository.findByName(executionName);
	}

	@Override
	public ExecutionRun findLastExecutionRun(Execution execution) {
		List<ExecutionRun> l = executionRunRepository.findLast(execution, new PageRequest(0, 1));
		return l.size() == 1 ? l.get(0) : null;
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#execution, 'ADMINISTRATION')")
	@Override
	@Transactional
	public Execution save(Execution execution) {
		return executionRepository.save(execution);
	}

	@Override
	public List<Execution> listExecutions() {
		return executionRepository.findAll();
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#execution, 'ADMINISTRATION')")
	@Override
	@Transactional
	public Execution delete(Execution execution) {
		// Execption thrown if there are Observations (which is okay).
		executionRunRepository.deleteByExecution(execution);
		executionRepository.delete(execution);
		execution.setId(null);
		return execution;
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#execution, 'ADMINISTRATION')")
	@Override
	@Transactional
	public void deleteObservations(ExecutionRun executionRun) {
		observationRepository.deleteByExecutionRun(executionRun);
	}

	@Override
	public Set<?> getValues(Dimension<?> dim) {
		if (dim instanceof JpaDimension) {
			JpaDimension jpaDim = (JpaDimension) dim;
			StringBuilder paQuery = new StringBuilder();
			paQuery.append("select distinct a.").append(jpaDim.getField()).append(" from ");
			paQuery.append(jpaDim.getEntity()).append(" a");
			if (StringUtils.isNotBlank(jpaDim.getCondition())) {
				paQuery.append(" where ").append(jpaDim.getCondition());
			}
			LOG.debug(paQuery);

			Query q = entityManager.createQuery(paQuery.toString());

			return new HashSet<Object>(q.getResultList());
		} else {
			// Dimension<?> dim2 = dimensionRepository.findOne(dim.getId());
			// dim2.getValues().size();
			return dim.getValues();
		}
	}

	@Override
	public long getSingleResult(String paQuery, Object... params) {
		LOG.debug(paQuery);
		Query q = entityManager.createQuery(paQuery, Long.class);

		for (int i = 0; i < params.length; i++) {
			LOG.debug("\t?" + (i + 1) + " = " + params[i]);
			q.setParameter(i + 1, params[i]);
		}
		Long res = (Long) q.getSingleResult();
		return res.longValue();
	}

	// readonly mode
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#execution, 'ADMINISTRATION')")
	@Override
	public List<Observation> execute(Execution execution) {
		List<Observation> results = new ArrayList<Observation>();
		execution = executionRepository.findOne(execution.getId());
		internalExecute(execution.query(), execution, results, 0, new ArrayList<Object>());
		return results;
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#execution, 'ADMINISTRATION')")
	@Override
	@Transactional
	public List<Observation> save(Execution execution, List<Observation> observations) {

		ExecutionRun executionRun = new ExecutionRun();
		executionRun.setExecution(execution);
		executionRun.setTimestamp(new Date());

		Map<String, Map<String, DimensionKey>> runCache = new HashMap<String, Map<String, DimensionKey>>();

		for (Observation obs : observations) {
			for (DimensionKey dk : obs.getDimensions()) {
				if (!runCache.containsKey(dk.getName())) {
					LOG.info("Loading map of values for " + dk.getName());
					List<Object[]> x = dimensionKeyRepository.loadMap(dk.getName());
					Map<String, DimensionKey> bar = new HashMap<String, DimensionKey>();
					for (Object[] foo : x) {
						bar.put((String) foo[0], (DimensionKey) foo[1]);
					}
					LOG.info("Map " + dk.getName() + " size=" + bar.size());
					LOG.info(bar);
					runCache.put(dk.getName(), bar);
				}
			}
		}

		for (Observation obs : observations) {
			Set<DimensionKey> dims = new HashSet<DimensionKey>();
			for (DimensionKey dk : obs.getDimensions()) {
				try {
					if (LOG.isDebugEnabled())
						LOG.debug("Finding dk: " + dk);
					DimensionKey existing = runCache.get(dk.getName()).get(dk.getValue());
					if (existing == null) {
						LOG.info("New dk: " + dk);
						dims.add(makeDimensionKey(dk));
					} else {
						dims.add(existing);
					}
				} catch (Throwable e) {
					LOG.error(e.getMessage());
					LOG.info("New dk: " + dk);
					dims.add(makeDimensionKey(dk));
				}
			}
			obs.setDimensions(dims);
			obs.setExecutionRun(executionRun);
		}

		// ExecutionRun previousRun = findLastExecutionRun(execution);
		// if (previousRun != null) {
		// LOG.info("Original observations size: " + observations.size());
		// removeSameResult(previousRun, observations);
		// LOG.info("Trimmed observations size: " + observations.size());
		// }

		if (observations.size() == 0) {
			LOG.warn("No observations to save. Not storing execution run.");
			return observations;
		}

		executionRunRepository.save(executionRun);
		return observationRepository.save(observations);
	}

	// private void removeSameResult(ExecutionRun previousRun, List<Observation>
	// observations) {
	// List<Observation> previousObservations =
	// observationRepository.findByExecutionRun(previousRun);
	// LOG.info("Got previous observations size=" +
	// previousObservations.size());
	// for (int i = observations.size() - 1; i >= 0; i--) {
	// if (!observationChanged(observations.get(i), previousObservations)) {
	// if (LOG.isDebugEnabled())
	// LOG.debug("Removing unchanged observation: " + observations.get(i));
	// observations.remove(i);
	// }
	// }
	// }
	// private boolean observationChanged(Observation observation,
	// List<Observation> observations) {
	// Set<DimensionKey> dims = observation.getDimensions();
	// Observation previous = findObservationByDimensionKeys(observations,
	// dims);
	// if (previous == null) {
	// // No matching previous observation
	// if (LOG.isDebugEnabled())
	// LOG.debug("No match found");
	// return true;
	// }
	// if (previous.getValue() != observation.getValue()) {
	// // Value is different
	// if (LOG.isDebugEnabled())
	// LOG.debug("Values are different " + previous + "!=" + observation);
	// return true;
	// }
	// // No change
	// return false;
	// }
	//
	// private Observation findObservationByDimensionKeys(List<Observation>
	// observations, Set<DimensionKey> dims) {
	// for (Observation obs : observations) {
	// if (obs.hasDimensionKeys(dims)) {
	// return obs;
	// }
	// }
	// return null;
	// }

	@Transactional
	private DimensionKey makeDimensionKey(DimensionKey dk) {
		return dimensionKeyRepository.save(dk);
	}

	private void internalExecute(String paQuery, Execution paramExec, List<Observation> results, int depth, List<Object> params) {
		Dimension<?> dim = paramExec.getDimension(depth);
		if (dim == null) {
			// execute
			if (LOG.isDebugEnabled())
				LOG.debug("Executing: " + paQuery + " params=" + params);
			long res = getSingleResult(paQuery, params.toArray());
			printRes(res, paramExec, params.toArray(), results);
		} else {
			// Recurse
			Set<?> values = null;
			values = getValues(dim);
			for (Object val : values) {
				params.add(val);
				internalExecute(paQuery, paramExec, results, depth + 1, params);
				params.remove(depth);
			}
		}
	}

	private void printRes(long res, Execution paramExec, Object[] array, List<Observation> results) {
		KPIParameter parameter = paramExec.getParameter();
		if (LOG.isDebugEnabled()) {
			LOG.debug("Reporting result");
			LOG.debug(parameter.getName() + "=" + res);
			LOG.debug("Params:" + ArrayUtils.toString(array));
		}

		Observation observation = new Observation();
		observation.setValue(res);
		for (int i = 0; i < array.length; i++) {
			String name = paramExec.getDimension(i).getName();
			String value = array[i].toString();
			LOG.debug(" dk name=" + name + " val=" + array[i].toString());
			DimensionKey dk = dimensionKeyRepository.findByNameAndValue(name, value);
			if (dk == null) {
				dk = new DimensionKey();
				dk.setName(paramExec.getDimension(i).getName());
				dk.setValue(array[i].toString());
			}
			if (LOG.isDebugEnabled())
				LOG.debug("\t\t" + dk);
			observation.getDimensions().add(dk);
		}

		if (LOG.isDebugEnabled())
			LOG.debug("OBSERVATION: " + observation);

		results.add(observation);
	}

	@Override
	public List<Observation> listObservations(ExecutionRun executionRun, Map<String, String> dimensionFilters, Pageable page) {
		List<Observation> res = null;
		if (dimensionFilters == null || dimensionFilters.isEmpty()) {
			LOG.debug("Dimension filters not provided");
			res = observationRepository.findByExecutionRun(executionRun, page);
		} else {
			Set<DimensionKey> dks = getDimensionKeys(dimensionFilters);
			LOG.debug("Got " + dks.size() + " dimension keys.");
			res = observationRepository.findObservations(executionRun, dks, page);
		}

		// Load lazy
		for (Observation o : res) {
			o.getDimensions().size();
		}
		return res;
	}
	
	@Override
	public List<Observation> listObservations(Execution execution, long dimensionKeyId, Pageable pageable) {
		DimensionKey dk=dimensionKeyRepository.findOne(dimensionKeyId);
		List<Observation> res = observationRepository.listObservationsByDimensionKey(execution, dk, pageable);

		// Load lazy
		for (Observation o : res) {
			o.getExecutionRun().getId();
			o.getDimensions().size();
		}
		return res;
	}

	private Set<DimensionKey> getDimensionKeys(Map<String, String> dimensionFilters) {
		// TODO needs .equals()?
		Set<DimensionKey> dks = new HashSet<DimensionKey>();
		for (String name : dimensionFilters.keySet()) {
			String value = dimensionFilters.get(name);
			if (value == null) {
				continue;
			} else {
				// Get one
				DimensionKey dk = dimensionKeyRepository.findByNameAndValue(name, value);
				if (dk != null)
					dks.add(dk);
			}
		}
		return dks;
	}

	@Override
	public List<ExecutionRun> listExecutionRuns(Execution execution, Pageable pageable) {
		return executionRunRepository.findLast(execution, pageable);
	}

	@Override
	public ExecutionRun getExecutionRun(long runId) {
		return executionRunRepository.findOne(runId);
	}
}
