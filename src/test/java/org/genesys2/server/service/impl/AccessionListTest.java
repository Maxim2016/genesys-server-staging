/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.service.AccessionListService;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { HazelcastConfig.class, JpaDataConfig.class, AccessionListTest.Config.class }, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
@Ignore
public class AccessionListTest {
	
	public static class Config {

		@Bean
		public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
			HazelcastCacheManager cm = new HazelcastCacheManager(hazelcastInstance);
			return cm;
		}

		@Bean
		public AccessionListServiceImpl accessionListService() {
			return new AccessionListServiceImpl();
		}

		
	}

	@Autowired
	private AccessionListService accessionListService;

	@Test
	public void testAccessionList1() {
		AccessionList accessionList=new AccessionList();
		accessionList.setTitle("List1");
		accessionListService.save(accessionList);
		assertThat("AccessionList is missing UUID", accessionList.getUuid(), notNullValue());
		
		AccessionList loaded = accessionListService.getList(accessionList.getUuid());
		assertThat("AccessionList could not be loaded by UUID", loaded, notNullValue());
		
	}
}
