/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.regex.Matcher;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class LocaleURLFilter implements Filter {
	private static final Logger LOG = Logger.getLogger(LocaleURLFilter.class);

	private static final LocaleURLMatcher localeUrlMatcher = new LocaleURLMatcher();
	public static final String REQUEST_LOCALE_ATTR = LocaleURLFilter.class.getName() + ".LOCALE";
	private static final String REQUEST_INTERNAL_URL = LocaleURLFilter.class.getName() + ".INTERNALURL";

	private String[] allowedLocales = null;
	private Locale defaultLocale;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String excludePaths = filterConfig.getInitParameter("exclude-paths");
		if (StringUtils.isNotBlank(excludePaths)) {
			String[] ex = excludePaths.split("\\s+");
			for (String e : ex) {
				LOG.info("Excluding path: " + e);
			}
			localeUrlMatcher.setExcludedPaths(ex);
		}

		String defaultLocale = filterConfig.getInitParameter("default-locale");
		if (defaultLocale != null) {
			this.defaultLocale = Locale.forLanguageTag(defaultLocale);
		} else {
			this.defaultLocale = Locale.getDefault();
		}
		LOG.info("Using default locale: " + this.defaultLocale);

		String allowedLocales = filterConfig.getInitParameter("allowed-locales");
		if (StringUtils.isNotBlank(allowedLocales)) {
			String[] ex = allowedLocales.split("\\s+");
			for (String l : ex) {
				LOG.info("Allowed locale: " + l);
			}
			this.allowedLocales = ex;
		}
	}

	@Override
	public void destroy() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Destroying LocaleURLFilter");
		}
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
		final String url = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());

		if (localeUrlMatcher.isExcludedPath(url)) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Incoming URL: " + url);
		}

		final Matcher matcher = localeUrlMatcher.matcher(url);
		if (matcher.matches()) {
			String urlLanguage = matcher.group(1);
			String remainingUrl = matcher.group(2);

			if (this.allowedLocales != null) {
				boolean localeAllowed = false;
				for (String allowedLocale : this.allowedLocales) {
					if (allowedLocale.equalsIgnoreCase(urlLanguage)) {
						localeAllowed = true;
						break;
					}
				}

				if (!localeAllowed) {
					LOG.warn("Locale not allowed. Temporary redirect to default locale.");
					httpResponse.sendRedirect(getInternalUrl(remainingUrl, httpRequest.getQueryString()));
					return;
				}
			}

			Locale urlLocale = Locale.forLanguageTag(urlLanguage);

			if (urlLocale.equals(this.defaultLocale)) {
				String defaultLocaleUrl = getInternalUrl(remainingUrl, httpRequest.getQueryString());
				LOG.warn("Default locale requested, permanent-redirect to " + defaultLocaleUrl);

				httpResponse.reset();
				httpResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
				httpResponse.setHeader("Location", defaultLocaleUrl);
				return;
			}

			httpRequest.setAttribute(REQUEST_LOCALE_ATTR, urlLocale);
			httpRequest.setAttribute(REQUEST_INTERNAL_URL, getInternalUrl(remainingUrl, httpRequest.getQueryString()));

			if (LOG.isDebugEnabled()) {
				LOG.debug("URL matches! lang=" + urlLanguage + " remaining=" + remainingUrl);
				LOG.debug("Country: " + urlLocale.getCountry() + " Lang: " + urlLocale.getLanguage() + " locale=" + urlLocale);

				Enumeration<String> attrNames = httpRequest.getAttributeNames();
				while (attrNames.hasMoreElements()) {
					String attrName = attrNames.nextElement();
					LOG.debug("Request attr " + attrName + " = " + httpRequest.getAttribute(attrName));
				}

				LOG.debug("Proxying request to remaining URL " + remainingUrl);
			}

			LocaleWrappedServletResponse localeResponse = new LocaleWrappedServletResponse(httpResponse, localeUrlMatcher, urlLanguage,
					defaultLocale.toLanguageTag());
			LocaleWrappedServletRequest localeRequest = new LocaleWrappedServletRequest(httpRequest, url, remainingUrl);

			// request.getRequestDispatcher(remainingUrl == null ? "/" :
			// remainingUrl).forward(servletRequest, localeResponse);
			filterChain.doFilter(localeRequest, localeResponse);
		} else {
			if (LOG.isDebugEnabled()) {
				LOG.debug("No match on url " + url);
			}
			httpRequest.setAttribute(REQUEST_INTERNAL_URL, getInternalUrl(url, httpRequest.getQueryString()));
			LocaleWrappedServletResponse localeResponse = new LocaleWrappedServletResponse(httpResponse, localeUrlMatcher, null,
					defaultLocale.toLanguageTag());
			filterChain.doFilter(servletRequest, localeResponse);
		}
	}

	private String getInternalUrl(String url, String queryString) {
		if (StringUtils.isBlank(queryString))
			return url;
		else
			return url + "?" + queryString;
	}

}
