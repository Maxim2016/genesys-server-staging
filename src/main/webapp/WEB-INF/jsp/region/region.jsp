<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><c:out value="${region.getName(pageContext.response.locale)}" /></title>
</head>
<body>
	<div class="informative-h1 row">
			<div class="col-md-12 col-sm-12">
				<h1>
					<c:out value="${region.getName(pageContext.response.locale)}" />
				</h1>
				<c:out value="${blurp.summary}" escapeXml="false" />
			</div>
	</div>

		<div class="results main-col-header">
			<h4>
				<a class="btn btn-default" href="<c:url value="/geo/regions" />"><spring:message code="region.show-all-regions" /></a>
				<c:if test="${region.parentRegion ne null}">
							<a class="btn btn-default" href="<c:url value="/geo/regions/${region.parentRegion.isoCode}"/>">
								<span class="glyphicon glyphicon-eye-open"></span>
								<span>
									<spring:message code="region.page.show.parent" arguments="${region.parentRegion.getName(pageContext.response.locale)}" />
								</span>
							</a>
				</c:if>
			</h4>
		</div>

	<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#region, 'ADMINISTRATION')">
		<a href="<c:url value="/geo/regions/${region.isoCode}/edit" />" class="close">
			<spring:message code="edit"/>
		</a>
	</security:authorize>

	<!-- Geographic region text -->	
	<cms:blurb blurb="${blurp}" />

	<c:if test="${subRegions ne null and subRegions.size() gt 0}">
		<h3><spring:message code="region.regions-in-region" arguments="${region.getName(pageContext.response.locale)}" /></h3>
		<ul class="funny-list">
		    <c:forEach items="${subRegions}" var="subRegion" varStatus="status">
		        <li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show" href="<c:url value="/geo/regions/${subRegion.isoCode}" />"><c:out value="${subRegion.getName(pageContext.response.locale)}" /></a></li>
		    </c:forEach>
		</ul>
	</c:if>

	<c:if test="${countries ne null and countries.size() gt 0}">
		<h3><spring:message code="region.countries-in-region" arguments="${region.getName(pageContext.response.locale)}" /></h3>
		<ul class="funny-list">
		    <c:forEach items="${countries}" var="country" varStatus="status">
		        <li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show ${not country.current ? 'disabled' : ''}" href="<c:url value="/geo/${country.code3}" />"><c:out value="${country.getName(pageContext.response.locale)}" /></a></li>
		    </c:forEach>
		</ul>
	</c:if>

</body>
</html>
