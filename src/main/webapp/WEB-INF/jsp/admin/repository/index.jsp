<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="menu.admin.repository.files" /></title>
</head>
<body>
	<!-- Any alerts? -->
	<gui:alert type="warning" display="${not empty errorMessage}">
		<c:out value="${errorMessage}" />
	</gui:alert>

	<div class="row">
		<div class="col-xs-2">
			<a href=".."
				class="form-control btn btn-primary ${currentPath eq '/' ? 'disabled' :''}">1UP</a>
		</div>
		<div class="col-xs-6">
			<select id="repositoryPathNav" class="form-control"
				name="repositoryPath">
				<option value="<c:url value="/admin/r/files/" />">ROOT</option>
				<option selected
					value="<c:url value="/admin/r/files${currentPath}" />"><c:out
						value="${currentPath}" /></option>
				<c:forEach items="${subPaths}" var="path">
					<option value="<c:url value="/admin/r/files${path}" />"><c:out
							value="${path}" /></option>
				</c:forEach>
			</select>
		</div>
		<div class="col-xs-4">
			<c:choose>
				<c:when test="${imageGallery ne null}">
					<a class="btn btn-default form-control"
						href="<c:url value="/admin/r/g${currentPath}" />"><spring:message
							code="repository.gallery.navigate" /></a>
				</c:when>
				<c:otherwise>
					<a class="btn btn-default form-control"
						href="<c:url value="/admin/r/g/edit"><c:param name="galleryPath" value="${currentPath}" /></c:url>"><spring:message
							code="repository.gallery.create-here" /></a>
				</c:otherwise>
			</c:choose>
		</div>
	</div>

	<table class="table table-striped">
		<thead>
			<tr>
				<th class="col-md-5 col-xs-7"><spring:message
						code="repository.file.title" /> <small><spring:message
							code="repository.file.originalFilename" /></small></th>
				<th class="col-md-3 hidden-sm hidden-xs"><spring:message
						code="repository.file.path" /></th>
				<th class="col-md-4 col-xs-5"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="file" items="${fileList}" varStatus="i">
				<tr>
					<td class="col-md-5 col-xs-7"><a
						href="<c:url value="/repository/d${file.path}${file.uuid}${file.extension}" />">
							<c:out value="${file.title}" /> <small><c:out value="${file.originalFilename}" /></small>
					</a></td>
					<td class="col-md-3 hidden-sm hidden-xs"><c:out
							value="${file.path}" /></td>
					<td class="col-md-4 col-xs-5 text-right">
						<form action="<c:url value="/admin/r/delete" />" method="post">
							<a
								href="<c:url value="/admin/r/edit"><c:param name="uuid" value="${file.uuid}" /></c:url>"
								class="btn btn-default"><spring:message code="edit" /></a> <input
								type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <input type="hidden" name="uuid"
								value="${file.uuid}" /> <input type="hidden"
								name="repositoryPath" value="${currentPath}" />
							<button type="submit" name="action" value="delete-file"
								class="btn btn-default confirm-delete">
								<spring:message code="delete" />
							</button>
						</form>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>


	<form
		action="<c:url value="/admin/r/upload"><c:param name="${_csrf.parameterName}" value="${_csrf.token}" /></c:url>"
		method="post" enctype="multipart/form-data" class="">

		<input type="hidden" name="repositoryPath" value="${currentPath}" />

		<div class="form-group">
			<input type="file" name="file" />
			<p class="help-block">
				Pick a file to upload to current path <b><c:out
						value="${currentPath}" /></b>
			</p>
		</div>
		<button type="submit" class="btn btn-primary">
			<spring:message code="file.upload-file" />
		</button>
	</form>

	<content tag="javascript"> <script type="text/javascript">
		$(document).ready(function() {
			$('#repositoryPathNav').change(function() {
				console.log(this.value);
				document.location.pathname = this.value;
			});

			$('.confirm-delete').click(function(ev) {
				if (!window
						.confirm('<spring:message code="prompt.confirm-delete" />')) {
					ev.stopPropagation();
					return false;
				}
			});
		});
	</script> </content>

</body>
</html>
