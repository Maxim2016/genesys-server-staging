/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.kpi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "kpiexecutiondimension")
public class ExecutionDimension extends BusinessModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5401855589899745004L;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "dimensionId")
	private Dimension<?> dimension;

	@Column(length = 100, nullable = true)
	private String link;
	@Column(length = 100, nullable = false)
	private String field;

	public void setDimension(Dimension<?> dimension) {
		this.dimension = dimension;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getField() {
		return field;
	}

	public Dimension<?> getDimension() {
		return dimension;
	}

	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "id=" + id + " link=" + link + " field=" + field + " dim=" + dimension.getName();
	}

}
