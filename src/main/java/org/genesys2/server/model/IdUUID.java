package org.genesys2.server.model;

import java.util.UUID;

public interface IdUUID {
	UUID getUuid();
}
