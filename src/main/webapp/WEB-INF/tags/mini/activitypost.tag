<%@ tag description="Display article" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="local" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/cms" %>
<%@ attribute name="activityPost" required="true"
	type="org.genesys2.server.model.impl.ActivityPost" %>

<div class="genesys-mini genesy-post">
	<div class="title">
		<c:out escapeXml="false" value="${activityPost.title}" />
	</div>
	<div class="url">
		<a
			href="<c:url value="/content/news/${activityPost.id}/${jspHelper.suggestUrlForText(activityPost.title)}" />">
			<c:url
				value="/content/news/${activityPost.id}/${jspHelper.suggestUrlForText(activityPost.title)}" />
		</a>
	</div>
	<div class="summary free-text">
		<c:out escapeXml="false" value="${activityPost.body}" />
	</div>
</div>
