/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import org.genesys2.server.model.impl.Project;
import org.genesys2.server.persistence.domain.ProjectRepository;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

@Service
@Transactional(readOnly = true)
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ContentService contentService;

	@Override
	public Page<Project> list(Pageable p) {
		return projectRepository.findAll(p);
	}

	@Override
	public Project getProjectById(Long id) {
		return projectRepository.findOne(id);
	}

	@Override
	public Project getProjectByCode(String code) {
		Project project = projectRepository.findByCode(code);
		project.getAccessionLists().size();
		return project;
	}

	@Override
	public List<Project> getAllProjects() {
		return projectRepository.findAll();
	}

	@Override
	public List<Project> getProjectsByName(String name) {
		return projectRepository.findByName(name);
	}

	@Override
	@PreAuthorize("#project.id==null or hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#project, 'ADMINISTRATION')")
	@Transactional
	public void saveProject(Project project) {
		projectRepository.save(project);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#project, 'ADMINISTRATION')")
	@Transactional
	public void deleteProject(Project project) {
		projectRepository.delete(project);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#project, 'ADMINISTRATION')")
	@Transactional
	public void updateBlurp(Project project, String textBody, String summary, Locale locale) {
		contentService.updateArticle(project, "blurp", null, textBody, summary, locale);
	}
}
