/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.security.AuthUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityContextUtil {
	private static final Log LOG = LogFactory.getLog(SecurityContextUtil.class);

	public static User getCurrentUser() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication != null && authentication.getPrincipal() instanceof AuthUserDetails) {
			return ((AuthUserDetails) authentication.getPrincipal()).getUser();
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("No User in security context, can't specify createdBy/lastUpdatedBy");
		}

		return null;
	}

	public static AuthUserDetails getAuthUser() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication == null || !(authentication.getPrincipal() instanceof AuthUserDetails) ? null : (AuthUserDetails) authentication.getPrincipal();
	}
}
