/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.VerificationToken;
import org.genesys2.server.persistence.domain.VerificationTokenRepository;
import org.genesys2.server.service.JPATokenStoreCleanup;
import org.genesys2.server.service.TokenVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;

@Service
@Transactional(readOnly = true)
public class TokenVerificationServiceImpl implements TokenVerificationService, JPATokenStoreCleanup {

	private static final Log LOG = LogFactory.getLog(TokenVerificationServiceImpl.class);

	private static final int HOURS_UNTIL_INVALID = 4;

	@Autowired
	private VerificationTokenRepository verificationTokenRepository;

	@Override
	@Transactional
	public VerificationToken generateToken(String tokenPurpose, String data) {
		VerificationToken token = new VerificationToken();
		token.setPurpose(tokenPurpose);
		// Store data
		token.setData(data);
		token.setKey(RandomStringUtils.randomAlphanumeric(4).toUpperCase());

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, HOURS_UNTIL_INVALID);
		Date validUntil = calendar.getTime();

		token.setValidUntil(validUntil);
		token = verificationTokenRepository.save(token);
		return token;
	}

    @Override
    @Transactional
	public void cancel(String tokenUuid) throws NoSuchVerificationTokenException {
		final VerificationToken verificationToken = verificationTokenRepository.findByUuid(tokenUuid);
		if (verificationToken == null) {
			LOG.warn("Canceling verification token failed. No such verification token " + tokenUuid);
			throw new NoSuchVerificationTokenException();
		} else {
			LOG.warn("Canceling verification token " + tokenUuid);
			verificationTokenRepository.delete(verificationToken);
		}
	}

	@Override
	@Transactional
	public VerificationToken consumeToken(String purpose, String tokenUuid, String key) throws NoSuchVerificationTokenException, TokenExpiredException {
		final VerificationToken verificationToken = verificationTokenRepository.findByPurposeAndUuid(purpose, tokenUuid);
		if (verificationToken == null) {
			LOG.warn("No such verification token " + tokenUuid + " key=" + key);
			throw new NoSuchVerificationTokenException();
		}

		if (!verificationToken.getKey().equals(key)) {
			LOG.error("Verification key invalid for token=" + verificationToken.getUuid() + " providedKey=" + key);
			throw new NoSuchVerificationTokenException();
		}

		Date now = Calendar.getInstance().getTime();
		if (verificationToken.getValidUntil().before(now)) {
			LOG.error("Verification token=" + verificationToken.getUuid() + " key=" + key + " has expired");
			throw new TokenExpiredException();
		}

		// Consume token
		verificationTokenRepository.delete(verificationToken);
		return verificationToken;
	}


	/**
	 * Cleanup executed every 10 minutes
	 */
	@Override
	@Transactional
	@Scheduled(fixedDelay = 600000)
	public void removeExpired() {
		final Date now = Calendar.getInstance().getTime();
		if (LOG.isTraceEnabled()) {
			LOG.trace("Removing expired verification tokens");
		}

		int count = verificationTokenRepository.deleteOlderThan(now);
		if (count > 0) {
			LOG.info("Removed expired verification tokens: " + count);
		}
	}
}
