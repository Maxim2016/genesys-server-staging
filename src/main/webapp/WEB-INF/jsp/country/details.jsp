<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="country.page.profile.title" arguments="${country.getName(pageContext.response.locale)}" argumentSeparator="|" /></title>

<local:content-headers description="${jspHelper.htmlToText(blurp.summary, 150)}" title="${country.getName(pageContext.response.locale)}" />
</head>

<body class="country-page">
<cms:informative-h1 title="country.page.profile.title" titleArgument="${country.getName(pageContext.response.locale)}" />

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<a href="<c:url value="/geo/${country.code3}/edit" />" class="close"> <spring:message code="edit" />
		</a>
	</security:authorize>


<div class="row map-wrapper dark-background">
		<div class="country-info-wrapper clearfix">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 country-info collect-info">
					<h1><c:out value="${country.getName(pageContext.response.locale)}" /></h1>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="geo.country" /></p></div>
						<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8"><p><a href="<c:url value="/geo" />"><spring:message code="country.page.list.title"/></a> <span class="glyphicon glyphicon-menu-right"></span> <c:out value="${country.getName(pageContext.response.locale)}" /></p></div>
					</div>
					<div class="row">
					<c:if test="${country.wikiLink ne null}">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="country.more-information" /></p></div>
							<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8"><a target="_blank" rel="nofollow" href="<c:out value="${country.wikiLink}" />"><c:out value="${country.wikiLink}" /></a></div>
					</c:if>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="country.stat.accessionCount" /></p></div>
						<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8">
							<p><span><c:out value="${countByOrigin}" /></span></p>
							<p class="buttons-container pull-right">
								<a class="btn btn-primary" title="" href="<c:url value="/geo/${country.code3}/data" />"> <span class="glyphicon glyphicon-list"></span> <spring:message code="view.accessions" /></a>
								<a class="btn btn-default" id="mapLink" href="<c:url value="/geo/${country.code3}/accessionmap" />"><span class="glyphicon glyphicon-globe"></span><span><spring:message code="maps.view-map"/></span></a>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4">ISO:</div>
						<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8">
							<span>ISO-3166 3-alpha: <c:out value="${country.code3}" />, </span>
							<span>ISO-3166 2-alpha: <c:out value="${country.code2}" /></span>
						</div>
					</div>
					<div class="row buttons-wrapper">
						<a class="btn btn-primary" title="" href="<c:url value="/geo/${country.code3}/data" />"> <span class="glyphicon glyphicon-list"></span> <spring:message code="view.accessions" /></a>
						<a class="btn btn-default" id="mapLink" href="<c:url value="/explore/map"><c:param name="filter">${jsonFilter}</c:param></c:url>"><span class="glyphicon glyphicon-globe"></span><span><spring:message code="maps.view-map"/></span></a>
					</div>
				</div>

			<%-- Show map? --%>
			<c:forEach items="${genesysInstitutes}" var="faoInstitute" varStatus="status">
				<c:if test="${faoInstitute.latitude ne null and faoInstitute.longitude ne null}">
					<c:set value="true" var="showMap" />
				</c:if>
			</c:forEach>
			<c:if test="${showMap!=null && showMap}">
				<div class="row map-container" style="">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="map" class="gis-map"></div>
					</div>
				</div>
			</c:if>
	</div>
</div>

<cms:blurb blurb="${blurp}" />

<!-- Is country current? -->
<gui:alert type="info" display="${not country.current}">
	<spring:message code="country.page.not-current" />
	<c:if test="${country.replacedBy != null}">
		<spring:message code="country.replaced-by" arguments="${country.replacedBy.code3}" />
		<a href="${country.replacedBy.code3}"><c:out value="${country.replacedBy.getName(pageContext.response.locale)}" /></a>
	</c:if>	
</gui:alert>

<gui:alert type="info" display="${itpgrfa != null and itpgrfa.contractingParty=='Yes'}">
	<spring:message code="country.is-itpgrfa-contractingParty" arguments="${country.getName(pageContext.response.locale)}" argumentSeparator="|" />
</gui:alert>
	
	
	<%--<h3><spring:message code="country.statistics" /></h3>--%>

<div class="collect-info inst-accs">
	<h4 class="row section-heading"><spring:message code="country.stat.countByLocation" arguments="${countByLocation}" /></h4>
		<div class="section-inner-content clearfix">
			<c:forEach items="${genesysInstitutes}" var="faoInstitute" varStatus="status">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 ${status.count % 2 == 0 ? 'even' : 'odd'}">
						<span class="index-number">${status.count + pagedData.size * pagedData.number}</span>
						<a class="" href="<c:url value="/wiews/${faoInstitute.code}" />"><b><c:out value="${faoInstitute.code}" /></b> <c:out value="${faoInstitute.fullName}" /></a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-3 col-xs-12 ${status.count % 2 == 0 ? 'even' : 'odd'}">
						<p><spring:message code="faoInstitute.accessionCount" arguments="${faoInstitute.accessionCount}" /></p>
					</div>
				</div>
			</c:forEach>
		</div>
</div>

<div class="collect-info inst-wiews">
	<h4 class="row section-heading"><spring:message code="country.page.faoInstitutes" arguments="${faoInstitutes.size()}" /></h4>
		<div class="section-inner-content clearfix">
			<c:forEach items="${faoInstitutes}" var="faoInstitute" varStatus="status">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ${status.count % 2 == 0 ? 'even' : 'odd'}">
						<span class="index-number">${status.count + pagedData.size * pagedData.number}</span>
						<a class="show" href="<c:url value="/wiews/${faoInstitute.code}" />"><b><c:out value="${faoInstitute.code}" /></b> <c:out value="${faoInstitute.fullName}" /></a>
					</div>
				</div>
			</c:forEach>
		</div>
</div>




<c:if test="${showMap!=null and showMap}">
<content tag="javascript">
	<script type="text/javascript">
		jQuery(document).ready(function() {
			var map=GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
				maxZoom: 6, /* WIEWS does not provide enough detail */
				zoom: 6
			}, function (opts, map) {
				var bounds=new GenesysMaps.BoundingBox();
				var marker;
				<c:forEach items="${genesysInstitutes}" var="faoInstitute" varStatus="status">
				<c:if test="${faoInstitute.latitude ne null and faoInstitute.longitude ne null}">
				marker = L.marker([${faoInstitute.latitude}, ${faoInstitute.longitude}]).addTo(map);
				marker.bindPopup("<a href='<c:url value="/wiews/${faoInstitute.code}" />'><c:out value="${faoInstitute.fullName}" /></a>");
				bounds.add([${faoInstitute.latitude}, ${faoInstitute.longitude}]);
				</c:if>
				</c:forEach>
				map.fitBounds(bounds.getBounds());
			});
		});
	</script>
</content>
</c:if>

</body>
</html>