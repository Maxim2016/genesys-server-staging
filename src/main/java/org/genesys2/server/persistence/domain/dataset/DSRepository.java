package org.genesys2.server.persistence.domain.dataset;

import java.util.UUID;

import org.genesys2.server.model.dataset.DS;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DSRepository extends JpaRepository<DS, Long> {

	DS getByUuid(UUID uuid);

	DS findByUuid(UUID uuid);

}
