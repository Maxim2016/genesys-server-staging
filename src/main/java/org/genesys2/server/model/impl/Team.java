/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.servlet.controller.rest.serialization.TeamSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * A team is a group of users, allowed to manage data in the system. Implements
 * the {@link AclAwareModel} so that ACL entries are managed.
 *
 * @author matijaobreza
 */
@Entity()
@Table(name = "team", uniqueConstraints = { @UniqueConstraint(name = "team_UC_name", columnNames = { "name" }) })
@JsonSerialize(using = TeamSerializer.class)
public class Team extends VersionedAuditedModel implements AclAwareModel {
	private static final long serialVersionUID = -6992621329254944604L;

	@Column(length = 36, unique = true)
	private String uuid;

	@Column(nullable = false, unique = true, length = 200)
	private String name;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "teammember", joinColumns = @JoinColumn(name = "teamId"), inverseJoinColumns = @JoinColumn(name = "userId"))
	private Set<User> members;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "teaminstitute", joinColumns = @JoinColumn(name = "teamId"), inverseJoinColumns = @JoinColumn(name = "instituteId"))
	@OrderBy("code")
	private List<FaoInstitute> institutes = new ArrayList<FaoInstitute>();

	@PrePersist
	void ensureUUID() {
		if (this.uuid == null) {
			this.uuid = UUID.randomUUID().toString();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get {@link User}s that are members of this team.
	 *
	 * @return
	 */
	public Set<User> getMembers() {
		return members;
	}

	public void setMembers(Set<User> members) {
		this.members = members;
	}

	/**
	 * Get {@link FaoInstitute}s that this {@link Team} is allowed to manage.
	 *
	 * @return
	 */
	public List<FaoInstitute> getInstitutes() {
		return institutes;
	}

	public void setInstitutes(List<FaoInstitute> institutes) {
		this.institutes = institutes;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return "Team " + this.name + " id=" + this.id;
	}
}
