/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.model.impl.Project;
import org.genesys2.server.service.AccessionListService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.DownloadService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.ProjectService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.SearchException;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/project")
public class ProjectController extends BaseController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private AccessionListService accessionListService;

	@Autowired
	private ElasticService elasticService;

	@Autowired
	ContentService contentService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private DownloadService downloadService;
	
	
	@RequestMapping({ "/", "" })
	public String projects() {
		return "redirect:/project/list";
	}

	@RequestMapping("/list")
	public String listProjects(ModelMap modelMap, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		Page<Project> projects = projectService.list(new PageRequest(page - 1, 20, new Sort("name")));

		modelMap.addAttribute("pagedData", projects);

		return "/project/index";
	}

	@RequestMapping("/{code}")
	public String viewProject(ModelMap model, @PathVariable(value = "code") String code) {
		Project project = projectService.getProjectByCode(code);
		if (project != null) {
			model.addAttribute("project", project);
			List<AccessionList> accessionLists = accessionListService.getLists(project.getAccessionLists());
			model.addAttribute("accessionLists", accessionLists);
			model.addAttribute("blurp", contentService.getArticle(project, "blurp", getLocale()));

			// Statistics
			int countByProject = accessionListService.distinctCount(accessionLists);
			model.addAttribute("countByProject", countByProject);

			if (countByProject > 0) {
				AppliedFilters filters = projectFilters(project);
				model.addAttribute("jsonFilter", filters.toString());

				try {
					model.addAttribute("statisticsCrop", elasticService.termStatisticsAuto(filters, FilterConstants.CROPS, 5));
					model.addAttribute("statisticsGenus", elasticService.termStatisticsAuto(filters, FilterConstants.TAXONOMY_GENUS, 5));
					model.addAttribute("statisticsTaxonomy", elasticService.termStatisticsAuto(filters, FilterConstants.TAXONOMY_GENUSSPECIES, 5));
					model.addAttribute("statisticsOrigCty", elasticService.termStatisticsAuto(filters, FilterConstants.ORGCTY_ISO3, 100));
				} catch (SearchException e) {
					_logger.warn(e.getMessage());
				}
			}

		} else {
			throw new ResourceNotFoundException("No project with code " + code);
		}
		return "/project/view";
	}

	private AppliedFilters projectFilters(Project project) {
		AppliedFilters filters = new AppliedFilters();

		AppliedFilter listsFilter = new AppliedFilter().setFilterName(FilterConstants.LISTS);
		for (UUID uuid : project.getAccessionLists()) {
			listsFilter.addFilterValue(new FilterHandler.LiteralValueFilter(uuid));
		}
		filters.add(listsFilter);
		return filters;
	}

	@RequestMapping("/add-project")
	public String addProject(ModelMap modelMap) {
		modelMap.addAttribute("project", new Project());
		return "/project/edit";
	}

	@RequestMapping("/{code}/edit")
	public String editProject(ModelMap modelMap, @PathVariable(value = "code") String code) {
		viewProject(modelMap, code);
		return "/project/edit";
	}

	@RequestMapping("/update-project")
	public String update(ModelMap modelMap, @ModelAttribute("project") Project p, @RequestParam("blurp") String aboutBody,
			@RequestParam(value = "summary", required = false) String summary, @RequestParam(value = "accessionListTitle", required = false) List<String> titles) {

		_logger.debug("Updating project " + p.getCode());
		Project project = null;

		if (p.getId() != null)
			project = projectService.getProjectById(p.getId());

		if (project == null) {
			project = new Project();
		}

		project.setCode(p.getCode());
		project.setUrl(p.getUrl());
		project.setName(p.getName());

		Set<String> titlesWithoutDuplicates = new LinkedHashSet<String>(titles);
		List<UUID> projectAccessionLists = new ArrayList<UUID>();
		for (String title : titlesWithoutDuplicates) {
			if (StringUtils.isNotBlank(title)) {
				projectAccessionLists.add(accessionListService.getUUIDByTitle(title));
			}
		}
		project.setAccessionLists(projectAccessionLists);

		projectService.saveProject(project);
		projectService.updateBlurp(project, aboutBody, summary, getLocale());

		return "redirect:/project/" + p.getCode();
	}

	@ResponseBody
	@RequestMapping("/get-autocomplete")
	public String getAutocompletedListName(@RequestParam String prefix) {
		if (!prefix.equals("")) {
			List<AccessionList> sharedLists = accessionListService.findShared();
			for (AccessionList list : sharedLists) {
				String title = list.getTitle();
				if (title.startsWith(prefix) && !title.equals(prefix)) {
					return title.substring(prefix.length());
				}
			}
		}

		return StringUtils.EMPTY;
	}

	@RequestMapping("/{code}/data")
	public String viewProjectData(ModelMap model, @PathVariable(value = "code") String code) {
		Project project = projectService.getProjectByCode(code);
		if (project != null && project.getAccessionLists().size() > 0) {
			AppliedFilters filters = projectFilters(project);
			model.addAttribute("filter", filters.toString());
			return "redirect:/explore";
		} else {
			throw new ResourceNotFoundException();
		}
	}


	@RequestMapping("/{code}/data/map")
	public String viewProjectMap(ModelMap model, @PathVariable(value = "code") String code) {
		Project project = projectService.getProjectByCode(code);
		if (project != null && project.getAccessionLists().size() > 0) {
			AppliedFilters filters = projectFilters(project);
			model.addAttribute("filter", filters.toString());
			return "redirect:/explore/map";
		} else {
			throw new ResourceNotFoundException();
		}
	}

	@RequestMapping("/{code}/overview")
	public String overview(ModelMap model, @PathVariable(value = "code") String code) throws UnsupportedEncodingException {
		final Project project = projectService.getProjectByCode(code);
		if (project != null && project.getAccessionLists().size() > 0) {
			AppliedFilters filters = projectFilters(project);
			return "forward:/explore/overview?filter=" + URLEncoder.encode(filters.toString(), "UTF8");
		} else {
			throw new ResourceNotFoundException();
		}
	}
	

	@RequestMapping(value = "/{code}/download", method = RequestMethod.POST, params = { "dwca" })
	public void downloadDwca(ModelMap model, @PathVariable(value = "code") String code, HttpServletResponse response) throws IOException {
		final Project project = projectService.getProjectByCode(code);
		if (project == null || project.getAccessionLists().size() == 0) {
			throw new ResourceNotFoundException();
		}
		_logger.warn("Downloading DWCA of: " + project.getCode());

		// Create JSON filter
		AppliedFilters filters = projectFilters(project);

		// Write Darwin Core Archive to the stream.
		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-%1$s.zip\"", project.getCode()));

		final OutputStream outputStream = response.getOutputStream();
		genesysService.writeAccessions(filters, outputStream);
		response.flushBuffer();
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{code}/download", method = RequestMethod.POST, params = { "mcpd" })
	public void downloadMcpd(ModelMap model, @PathVariable(value = "code") String code, HttpServletResponse response) throws IOException {
		final Project project = projectService.getProjectByCode(code);
		if (project == null || project.getAccessionLists().size() == 0) {
			throw new ResourceNotFoundException();
		}
		_logger.warn("Downloading MCPD of: " + project.getCode());

		// Create JSON filter
		AppliedFilters filters = projectFilters(project);

		// Write MCPD to the stream.
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-accessions-%1$s.xlsx\"", project.getCode()));
		response.flushBuffer();

		final OutputStream outputStream = response.getOutputStream();
		try {
			downloadService.writeXlsxMCPD(filters, outputStream);
			response.flushBuffer();
		} catch (EOFException e) {
			_logger.warn("Download was aborted", e);
		}
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{code}/download", method = RequestMethod.POST, params = { "pdci" })
	public void downloadPdci(ModelMap model, @PathVariable(value = "code") String code, HttpServletResponse response) throws IOException {
		final Project project = projectService.getProjectByCode(code);
		if (project == null || project.getAccessionLists().size() == 0) {
			throw new ResourceNotFoundException();
		}
		_logger.warn("Downloading PDCI of: " + project.getCode());

		// Create JSON filter
		AppliedFilters filters = projectFilters(project);

		// Write MCPD to the stream.
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.addHeader("Content-Disposition", String.format("attachment; filename=\"genesys-pdci-%1$s.xlsx\"", project.getCode()));
		response.flushBuffer();

		final OutputStream outputStream = response.getOutputStream();
		try {
			downloadService.writeXlsxPDCI(filters, outputStream);
			response.flushBuffer();
		} catch (EOFException e) {
			_logger.warn("Download was aborted", e);
		}
	}
	
	protected Locale getLocale() {
		return LocaleContextHolder.getLocale();
	}
}
