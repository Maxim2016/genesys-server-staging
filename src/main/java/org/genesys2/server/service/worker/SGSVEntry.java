/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import org.genesys2.server.model.impl.AccessionIdentifier3;

class SGSVEntry implements AccessionIdentifier3 {
	String instCode;
	String acceNumb;
	String origCty;
	String genus;
	String species;
	String subtAuthor;
	String subtaxa;
	String spAuthor;
	String fullTaxa;
	String boxNo;
	String depositDate;
	Float quantity;
	long unitId;
	String acceUrl;

	public SGSVEntry() {
	}

	public SGSVEntry(String[] entry) {
		instCode = entry[1];
		acceNumb = entry[4];
		fullTaxa = entry[5];
		origCty = entry[12];
		genus = entry[16];
		species = entry[17];
		spAuthor = entry[25];
		subtaxa = entry[26];
		depositDate = entry[20];
		if (entry[7] != null) {
			quantity = Float.parseFloat(entry[7]);
		}
		boxNo = entry[2];
		acceUrl = entry[11];
		unitId = Long.parseLong(entry[0]);
	}

	@Override
	public String getHoldingInstitute() {
		return instCode;
	}

	@Override
	public String getAccessionName() {
		return acceNumb;
	}

	@Override
	public String getGenus() {
		return genus;
	}

	@Override
	public String toString() {
		return "SGSVEntry " + instCode + " " + acceNumb + " " + genus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((instCode == null) ? 0 : instCode.hashCode());
		result = prime * result + ((acceNumb == null) ? 0 : acceNumb.hashCode());
		result = prime * result + ((genus == null) ? 0 : genus.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SGSVEntry other = (SGSVEntry) obj;
		if (instCode == null) {
			if (other.instCode != null)
				return false;
		} else if (!instCode.equals(other.instCode))
			return false;
		if (acceNumb == null) {
			if (other.acceNumb != null)
				return false;
		} else if (!acceNumb.equals(other.acceNumb))
			return false;
		if (genus == null) {
			if (other.genus != null)
				return false;
		} else if (!genus.equals(other.genus))
			return false;
		return true;
	}
}
