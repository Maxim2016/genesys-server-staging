/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import java.io.File;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.genesys2.server.servlet.filter.NewGUIFilter;
import org.genesys2.server.servlet.filter.NewGUIFilter.LocalGUIVersion;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Using the GUI version captured in {@link NewGUIFilter}, this view resolver checks for 
 * view (.jsp) files in {@code getPrefix() + "/" + LocalGUIVersion.get() + viewName + getSuffix()} location.
 * For example the /admin/index view will be resolved to /WEB-INF/jsp/admin/index.jsp with the 
 * standard resolver. 
 * 
 * The {@code NewGUIViewResolver} first checks if the versioned view exists at {@code /WEB-INF/jsp/1/admin/index.jsp}
 * and returns /1/admin/index if the relevant .jsp file exists.
 * 
 * @author Matija Obreza
 *
 */
public class NewGUIViewResolver extends InternalResourceViewResolver {
	private static final Logger LOG = Logger.getLogger(NewGUIViewResolver.class);

	@Override
	protected Object getCacheKey(String viewName, Locale locale) {
		if (LocalGUIVersion.get() == 0) {
			return super.getCacheKey(viewName, locale);
		}
		
		File file = new File(getServletContext().getRealPath(getPrefix() + "/" + LocalGUIVersion.get() + viewName + getSuffix()));
		if (LOG.isDebugEnabled()) {
			LOG.debug("Checking versioned view in " + file.getAbsolutePath());
		}
		if (file.exists()) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Using versioned view at /" + LocalGUIVersion.get() + viewName);
			}
			return super.getCacheKey("/" + LocalGUIVersion.get() + viewName, locale);
		} else {
			if (LOG.isInfoEnabled()) {
				LOG.info("Versioned view not found at /" + LocalGUIVersion.get() + viewName);
			}
		}
		
		return super.getCacheKey(viewName, locale);
	}

	@Override
	protected View createView(String viewName, Locale locale) throws Exception {
		if (LocalGUIVersion.get() == 0) {
			if (LOG.isTraceEnabled()) {
				LOG.trace("LocalGUIVersion.get() returns 0");
			}
			return super.createView(viewName, locale);
		}

		// If this resolver is not supposed to handle the given view,
		// return null to pass on to the next resolver in the chain.
		if (!canHandle(viewName, locale)) {
			return null;
		}
		// Check for special "redirect:" prefix.
		if (viewName.startsWith(REDIRECT_URL_PREFIX)) {
			super.createView(viewName, locale);
		}
		// Check for special "forward:" prefix.
		if (viewName.startsWith(FORWARD_URL_PREFIX)) {
			super.createView(viewName, locale);
		}

		// Check for getPrefix() + LocalGUIVersion.get() + viewName +
		// getSuffix() file
		File file = new File(getServletContext().getRealPath(getPrefix() + "/" + LocalGUIVersion.get() + viewName + getSuffix()));
		if (LOG.isDebugEnabled()) {
			LOG.debug("Checking versioned view in " + file.getAbsolutePath());
		}
		if (file.exists()) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Using versioned view at /" + LocalGUIVersion.get() + viewName);
			}
			return super.createView("/" + LocalGUIVersion.get() + viewName, locale);
		} else {
			if (LOG.isInfoEnabled()) {
				LOG.info("Versioned view not found at /" + LocalGUIVersion.get() + viewName);
			}
		}

		// Else fall back to superclass implementation: calling loadView.
		return super.createView(viewName, locale);
	}
}
