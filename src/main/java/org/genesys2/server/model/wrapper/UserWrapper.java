/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.wrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.genesys2.server.model.UserRole;

public class UserWrapper {

	private Long id;

	private String email;

	private String password;

	private String name;

	private Set<UserRole> roles;

	private List<KeyValueWrapper> userGroups = new ArrayList<KeyValueWrapper>();

	private List<KeyValueWrapper> organizations = new ArrayList<KeyValueWrapper>();

	private List<KeyValueWrapper> userGroupsAvailable = new ArrayList<KeyValueWrapper>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<UserRole> roles) {
		this.roles = roles;
	}

	public List<KeyValueWrapper> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<KeyValueWrapper> userGroups) {
		this.userGroups = userGroups;
	}

	public List<KeyValueWrapper> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<KeyValueWrapper> organizations) {
		this.organizations = organizations;
	}

	public List<KeyValueWrapper> getUserGroupsAvailable() {
		return userGroupsAvailable;
	}

	public void setUserGroupsAvailable(List<KeyValueWrapper> userGroupsAvailable) {
		this.userGroupsAvailable = userGroupsAvailable;
	}
}
